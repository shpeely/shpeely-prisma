#!/bin/sh

set -e

echo "Deploying to prisma..."
prisma deploy --force

exec "$@"
