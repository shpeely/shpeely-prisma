import { GraphQlError } from '../errors'

export function getUserFromCtx(ctx) {
  const user = ctx.request.user

  if (!user) throw new Error(GraphQlError.LOGIN_REQUIRED)

  return user
}
