import { GraphQLResolveInfo, GraphQLSchema } from 'graphql'
import { IResolvers } from 'graphql-tools/dist/Interfaces'
import { Options } from 'graphql-binding'
import { makePrismaBindingClass, BasePrismaOptions } from 'prisma-binding'

export interface Query {
  tournamentMembers: <T = Array<TournamentMember | null>>(
    args: {
      where?: TournamentMemberWhereInput | null
      orderBy?: TournamentMemberOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  bggInfoes: <T = Array<BggInfo | null>>(
    args: {
      where?: BggInfoWhereInput | null
      orderBy?: BggInfoOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  files: <T = Array<File | null>>(
    args: {
      where?: FileWhereInput | null
      orderBy?: FileOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  gameresults: <T = Array<Gameresult | null>>(
    args: {
      where?: GameresultWhereInput | null
      orderBy?: GameresultOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  players: <T = Array<Player | null>>(
    args: {
      where?: PlayerWhereInput | null
      orderBy?: PlayerOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  scores: <T = Array<Score | null>>(
    args: {
      where?: ScoreWhereInput | null
      orderBy?: ScoreOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournaments: <T = Array<Tournament | null>>(
    args: {
      where?: TournamentWhereInput | null
      orderBy?: TournamentOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  invitations: <T = Array<Invitation | null>>(
    args: {
      where?: InvitationWhereInput | null
      orderBy?: InvitationOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  users: <T = Array<User | null>>(
    args: {
      where?: UserWhereInput | null
      orderBy?: UserOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournamentMember: <T = TournamentMember | null>(
    args: { where: TournamentMemberWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  bggInfo: <T = BggInfo | null>(
    args: { where: BggInfoWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  file: <T = File | null>(
    args: { where: FileWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  gameresult: <T = Gameresult | null>(
    args: { where: GameresultWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  player: <T = Player | null>(
    args: { where: PlayerWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  score: <T = Score | null>(
    args: { where: ScoreWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  tournament: <T = Tournament | null>(
    args: { where: TournamentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  invitation: <T = Invitation | null>(
    args: { where: InvitationWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  user: <T = User | null>(
    args: { where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  tournamentMembersConnection: <T = TournamentMemberConnection>(
    args: {
      where?: TournamentMemberWhereInput | null
      orderBy?: TournamentMemberOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  bggInfoesConnection: <T = BggInfoConnection>(
    args: {
      where?: BggInfoWhereInput | null
      orderBy?: BggInfoOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  filesConnection: <T = FileConnection>(
    args: {
      where?: FileWhereInput | null
      orderBy?: FileOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  gameresultsConnection: <T = GameresultConnection>(
    args: {
      where?: GameresultWhereInput | null
      orderBy?: GameresultOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  playersConnection: <T = PlayerConnection>(
    args: {
      where?: PlayerWhereInput | null
      orderBy?: PlayerOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  scoresConnection: <T = ScoreConnection>(
    args: {
      where?: ScoreWhereInput | null
      orderBy?: ScoreOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournamentsConnection: <T = TournamentConnection>(
    args: {
      where?: TournamentWhereInput | null
      orderBy?: TournamentOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  invitationsConnection: <T = InvitationConnection>(
    args: {
      where?: InvitationWhereInput | null
      orderBy?: InvitationOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  usersConnection: <T = UserConnection>(
    args: {
      where?: UserWhereInput | null
      orderBy?: UserOrderByInput | null
      skip?: Int | null
      after?: String | null
      before?: String | null
      first?: Int | null
      last?: Int | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  node: <T = Node | null>(
    args: { id: ID_Output },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
}

export interface Mutation {
  createTournamentMember: <T = TournamentMember>(
    args: { data: TournamentMemberCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createBggInfo: <T = BggInfo>(
    args: { data: BggInfoCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createFile: <T = File>(
    args: { data: FileCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createGameresult: <T = Gameresult>(
    args: { data: GameresultCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createPlayer: <T = Player>(
    args: { data: PlayerCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createScore: <T = Score>(
    args: { data: ScoreCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createTournament: <T = Tournament>(
    args: { data: TournamentCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createInvitation: <T = Invitation>(
    args: { data: InvitationCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createUser: <T = User>(
    args: { data: UserCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateTournamentMember: <T = TournamentMember | null>(
    args: {
      data: TournamentMemberUpdateInput
      where: TournamentMemberWhereUniqueInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateBggInfo: <T = BggInfo | null>(
    args: { data: BggInfoUpdateInput; where: BggInfoWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateFile: <T = File | null>(
    args: { data: FileUpdateInput; where: FileWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateGameresult: <T = Gameresult | null>(
    args: { data: GameresultUpdateInput; where: GameresultWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updatePlayer: <T = Player | null>(
    args: { data: PlayerUpdateInput; where: PlayerWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateScore: <T = Score | null>(
    args: { data: ScoreUpdateInput; where: ScoreWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateTournament: <T = Tournament | null>(
    args: { data: TournamentUpdateInput; where: TournamentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateInvitation: <T = Invitation | null>(
    args: { data: InvitationUpdateInput; where: InvitationWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  updateUser: <T = User | null>(
    args: { data: UserUpdateInput; where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteTournamentMember: <T = TournamentMember | null>(
    args: { where: TournamentMemberWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteBggInfo: <T = BggInfo | null>(
    args: { where: BggInfoWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteFile: <T = File | null>(
    args: { where: FileWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteGameresult: <T = Gameresult | null>(
    args: { where: GameresultWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deletePlayer: <T = Player | null>(
    args: { where: PlayerWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteScore: <T = Score | null>(
    args: { where: ScoreWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteTournament: <T = Tournament | null>(
    args: { where: TournamentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteInvitation: <T = Invitation | null>(
    args: { where: InvitationWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  deleteUser: <T = User | null>(
    args: { where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>
  upsertTournamentMember: <T = TournamentMember>(
    args: {
      where: TournamentMemberWhereUniqueInput
      create: TournamentMemberCreateInput
      update: TournamentMemberUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertBggInfo: <T = BggInfo>(
    args: {
      where: BggInfoWhereUniqueInput
      create: BggInfoCreateInput
      update: BggInfoUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertFile: <T = File>(
    args: {
      where: FileWhereUniqueInput
      create: FileCreateInput
      update: FileUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertGameresult: <T = Gameresult>(
    args: {
      where: GameresultWhereUniqueInput
      create: GameresultCreateInput
      update: GameresultUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertPlayer: <T = Player>(
    args: {
      where: PlayerWhereUniqueInput
      create: PlayerCreateInput
      update: PlayerUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertScore: <T = Score>(
    args: {
      where: ScoreWhereUniqueInput
      create: ScoreCreateInput
      update: ScoreUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertTournament: <T = Tournament>(
    args: {
      where: TournamentWhereUniqueInput
      create: TournamentCreateInput
      update: TournamentUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertInvitation: <T = Invitation>(
    args: {
      where: InvitationWhereUniqueInput
      create: InvitationCreateInput
      update: InvitationUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertUser: <T = User>(
    args: {
      where: UserWhereUniqueInput
      create: UserCreateInput
      update: UserUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyTournamentMembers: <T = BatchPayload>(
    args: {
      data: TournamentMemberUpdateManyMutationInput
      where?: TournamentMemberWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyBggInfoes: <T = BatchPayload>(
    args: {
      data: BggInfoUpdateManyMutationInput
      where?: BggInfoWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyFiles: <T = BatchPayload>(
    args: { data: FileUpdateManyMutationInput; where?: FileWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyGameresults: <T = BatchPayload>(
    args: {
      data: GameresultUpdateManyMutationInput
      where?: GameresultWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyPlayers: <T = BatchPayload>(
    args: {
      data: PlayerUpdateManyMutationInput
      where?: PlayerWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyScores: <T = BatchPayload>(
    args: {
      data: ScoreUpdateManyMutationInput
      where?: ScoreWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyTournaments: <T = BatchPayload>(
    args: {
      data: TournamentUpdateManyMutationInput
      where?: TournamentWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyInvitations: <T = BatchPayload>(
    args: {
      data: InvitationUpdateManyMutationInput
      where?: InvitationWhereInput | null
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyUsers: <T = BatchPayload>(
    args: { data: UserUpdateManyMutationInput; where?: UserWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyTournamentMembers: <T = BatchPayload>(
    args: { where?: TournamentMemberWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyBggInfoes: <T = BatchPayload>(
    args: { where?: BggInfoWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyFiles: <T = BatchPayload>(
    args: { where?: FileWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyGameresults: <T = BatchPayload>(
    args: { where?: GameresultWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyPlayers: <T = BatchPayload>(
    args: { where?: PlayerWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyScores: <T = BatchPayload>(
    args: { where?: ScoreWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyTournaments: <T = BatchPayload>(
    args: { where?: TournamentWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyInvitations: <T = BatchPayload>(
    args: { where?: InvitationWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyUsers: <T = BatchPayload>(
    args: { where?: UserWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
}

export interface Subscription {
  tournamentMember: <T = TournamentMemberSubscriptionPayload | null>(
    args: { where?: TournamentMemberSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  bggInfo: <T = BggInfoSubscriptionPayload | null>(
    args: { where?: BggInfoSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  file: <T = FileSubscriptionPayload | null>(
    args: { where?: FileSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  gameresult: <T = GameresultSubscriptionPayload | null>(
    args: { where?: GameresultSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  player: <T = PlayerSubscriptionPayload | null>(
    args: { where?: PlayerSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  score: <T = ScoreSubscriptionPayload | null>(
    args: { where?: ScoreSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  tournament: <T = TournamentSubscriptionPayload | null>(
    args: { where?: TournamentSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  invitation: <T = InvitationSubscriptionPayload | null>(
    args: { where?: InvitationSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
  user: <T = UserSubscriptionPayload | null>(
    args: { where?: UserSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>
}

export interface Exists {
  TournamentMember: (where?: TournamentMemberWhereInput) => Promise<boolean>
  BggInfo: (where?: BggInfoWhereInput) => Promise<boolean>
  File: (where?: FileWhereInput) => Promise<boolean>
  Gameresult: (where?: GameresultWhereInput) => Promise<boolean>
  Player: (where?: PlayerWhereInput) => Promise<boolean>
  Score: (where?: ScoreWhereInput) => Promise<boolean>
  Tournament: (where?: TournamentWhereInput) => Promise<boolean>
  Invitation: (where?: InvitationWhereInput) => Promise<boolean>
  User: (where?: UserWhereInput) => Promise<boolean>
}

export interface Prisma {
  query: Query
  mutation: Mutation
  subscription: Subscription
  exists: Exists
  request: <T = any>(
    query: string,
    variables?: { [key: string]: any }
  ) => Promise<T>
  delegate(
    operation: 'query' | 'mutation',
    fieldName: string,
    args: {
      [key: string]: any
    },
    infoOrQuery?: GraphQLResolveInfo | string,
    options?: Options
  ): Promise<any>
  delegateSubscription(
    fieldName: string,
    args?: {
      [key: string]: any
    },
    infoOrQuery?: GraphQLResolveInfo | string,
    options?: Options
  ): Promise<AsyncIterator<any>>
  getAbstractResolvers(filterSchema?: GraphQLSchema | string): IResolvers
}

export interface BindingConstructor<T> {
  new (options: BasePrismaOptions): T
}
/**
 * Type Defs
 */

const typeDefs = `type AggregateBggInfo {
  count: Int!
}

type AggregateFile {
  count: Int!
}

type AggregateGameresult {
  count: Int!
}

type AggregateInvitation {
  count: Int!
}

type AggregatePlayer {
  count: Int!
}

type AggregateScore {
  count: Int!
}

type AggregateTournament {
  count: Int!
}

type AggregateTournamentMember {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  """The number of nodes that have been affected by the Batch operation."""
  count: Long!
}

type BggInfo implements Node {
  bggid: Int!
  createdAt: DateTime!
  description: String!
  gameresults(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Gameresult!]
  id: ID!
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  updatedAt: DateTime!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime!
}

"""A connection to a list of items."""
type BggInfoConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [BggInfoEdge]!
  aggregate: AggregateBggInfo!
}

input BggInfoCreateInput {
  bggid: Int!
  description: String!
  id: ID
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime
  gameresults: GameresultCreateManyWithoutBggInfoInput
}

input BggInfoCreateOneWithoutGameresultsInput {
  create: BggInfoCreateWithoutGameresultsInput
  connect: BggInfoWhereUniqueInput
}

input BggInfoCreateWithoutGameresultsInput {
  bggid: Int!
  description: String!
  id: ID
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime
}

"""An edge in a connection."""
type BggInfoEdge {
  """The item at the end of the edge."""
  node: BggInfo!

  """A cursor for use in pagination."""
  cursor: String!
}

enum BggInfoOrderByInput {
  bggid_ASC
  bggid_DESC
  createdAt_ASC
  createdAt_DESC
  description_ASC
  description_DESC
  id_ASC
  id_DESC
  maxPlayers_ASC
  maxPlayers_DESC
  name_ASC
  name_DESC
  ownedBy_ASC
  ownedBy_DESC
  playTime_ASC
  playTime_DESC
  rank_ASC
  rank_DESC
  rating_ASC
  rating_DESC
  thumbnail_ASC
  thumbnail_DESC
  updatedAt_ASC
  updatedAt_DESC
  weight_ASC
  weight_DESC
  yearPublished_ASC
  yearPublished_DESC
  lastReload_ASC
  lastReload_DESC
}

type BggInfoPreviousValues {
  bggid: Int!
  createdAt: DateTime!
  description: String!
  id: ID!
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  updatedAt: DateTime!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime!
}

type BggInfoSubscriptionPayload {
  mutation: MutationType!
  node: BggInfo
  updatedFields: [String!]
  previousValues: BggInfoPreviousValues
}

input BggInfoSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [BggInfoSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [BggInfoSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [BggInfoSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: BggInfoWhereInput
}

input BggInfoUpdateInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload: DateTime
  gameresults: GameresultUpdateManyWithoutBggInfoInput
}

input BggInfoUpdateManyMutationInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

input BggInfoUpdateOneWithoutGameresultsInput {
  create: BggInfoCreateWithoutGameresultsInput
  connect: BggInfoWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: BggInfoUpdateWithoutGameresultsDataInput
  upsert: BggInfoUpsertWithoutGameresultsInput
}

input BggInfoUpdateWithoutGameresultsDataInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

input BggInfoUpsertWithoutGameresultsInput {
  update: BggInfoUpdateWithoutGameresultsDataInput!
  create: BggInfoCreateWithoutGameresultsInput!
}

input BggInfoWhereInput {
  """Logical AND on all given filters."""
  AND: [BggInfoWhereInput!]

  """Logical OR on all given filters."""
  OR: [BggInfoWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [BggInfoWhereInput!]
  bggid: Int

  """All values that are not equal to given value."""
  bggid_not: Int

  """All values that are contained in given list."""
  bggid_in: [Int!]

  """All values that are not contained in given list."""
  bggid_not_in: [Int!]

  """All values less than the given value."""
  bggid_lt: Int

  """All values less than or equal the given value."""
  bggid_lte: Int

  """All values greater than the given value."""
  bggid_gt: Int

  """All values greater than or equal the given value."""
  bggid_gte: Int
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  description: String

  """All values that are not equal to given value."""
  description_not: String

  """All values that are contained in given list."""
  description_in: [String!]

  """All values that are not contained in given list."""
  description_not_in: [String!]

  """All values less than the given value."""
  description_lt: String

  """All values less than or equal the given value."""
  description_lte: String

  """All values greater than the given value."""
  description_gt: String

  """All values greater than or equal the given value."""
  description_gte: String

  """All values containing the given string."""
  description_contains: String

  """All values not containing the given string."""
  description_not_contains: String

  """All values starting with the given string."""
  description_starts_with: String

  """All values not starting with the given string."""
  description_not_starts_with: String

  """All values ending with the given string."""
  description_ends_with: String

  """All values not ending with the given string."""
  description_not_ends_with: String
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  maxPlayers: Int

  """All values that are not equal to given value."""
  maxPlayers_not: Int

  """All values that are contained in given list."""
  maxPlayers_in: [Int!]

  """All values that are not contained in given list."""
  maxPlayers_not_in: [Int!]

  """All values less than the given value."""
  maxPlayers_lt: Int

  """All values less than or equal the given value."""
  maxPlayers_lte: Int

  """All values greater than the given value."""
  maxPlayers_gt: Int

  """All values greater than or equal the given value."""
  maxPlayers_gte: Int
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  ownedBy: Int

  """All values that are not equal to given value."""
  ownedBy_not: Int

  """All values that are contained in given list."""
  ownedBy_in: [Int!]

  """All values that are not contained in given list."""
  ownedBy_not_in: [Int!]

  """All values less than the given value."""
  ownedBy_lt: Int

  """All values less than or equal the given value."""
  ownedBy_lte: Int

  """All values greater than the given value."""
  ownedBy_gt: Int

  """All values greater than or equal the given value."""
  ownedBy_gte: Int
  playTime: Int

  """All values that are not equal to given value."""
  playTime_not: Int

  """All values that are contained in given list."""
  playTime_in: [Int!]

  """All values that are not contained in given list."""
  playTime_not_in: [Int!]

  """All values less than the given value."""
  playTime_lt: Int

  """All values less than or equal the given value."""
  playTime_lte: Int

  """All values greater than the given value."""
  playTime_gt: Int

  """All values greater than or equal the given value."""
  playTime_gte: Int
  rank: Int

  """All values that are not equal to given value."""
  rank_not: Int

  """All values that are contained in given list."""
  rank_in: [Int!]

  """All values that are not contained in given list."""
  rank_not_in: [Int!]

  """All values less than the given value."""
  rank_lt: Int

  """All values less than or equal the given value."""
  rank_lte: Int

  """All values greater than the given value."""
  rank_gt: Int

  """All values greater than or equal the given value."""
  rank_gte: Int
  rating: Float

  """All values that are not equal to given value."""
  rating_not: Float

  """All values that are contained in given list."""
  rating_in: [Float!]

  """All values that are not contained in given list."""
  rating_not_in: [Float!]

  """All values less than the given value."""
  rating_lt: Float

  """All values less than or equal the given value."""
  rating_lte: Float

  """All values greater than the given value."""
  rating_gt: Float

  """All values greater than or equal the given value."""
  rating_gte: Float
  thumbnail: String

  """All values that are not equal to given value."""
  thumbnail_not: String

  """All values that are contained in given list."""
  thumbnail_in: [String!]

  """All values that are not contained in given list."""
  thumbnail_not_in: [String!]

  """All values less than the given value."""
  thumbnail_lt: String

  """All values less than or equal the given value."""
  thumbnail_lte: String

  """All values greater than the given value."""
  thumbnail_gt: String

  """All values greater than or equal the given value."""
  thumbnail_gte: String

  """All values containing the given string."""
  thumbnail_contains: String

  """All values not containing the given string."""
  thumbnail_not_contains: String

  """All values starting with the given string."""
  thumbnail_starts_with: String

  """All values not starting with the given string."""
  thumbnail_not_starts_with: String

  """All values ending with the given string."""
  thumbnail_ends_with: String

  """All values not ending with the given string."""
  thumbnail_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  weight: Float

  """All values that are not equal to given value."""
  weight_not: Float

  """All values that are contained in given list."""
  weight_in: [Float!]

  """All values that are not contained in given list."""
  weight_not_in: [Float!]

  """All values less than the given value."""
  weight_lt: Float

  """All values less than or equal the given value."""
  weight_lte: Float

  """All values greater than the given value."""
  weight_gt: Float

  """All values greater than or equal the given value."""
  weight_gte: Float
  yearPublished: Int

  """All values that are not equal to given value."""
  yearPublished_not: Int

  """All values that are contained in given list."""
  yearPublished_in: [Int!]

  """All values that are not contained in given list."""
  yearPublished_not_in: [Int!]

  """All values less than the given value."""
  yearPublished_lt: Int

  """All values less than or equal the given value."""
  yearPublished_lte: Int

  """All values greater than the given value."""
  yearPublished_gt: Int

  """All values greater than or equal the given value."""
  yearPublished_gte: Int
  lastReload: DateTime

  """All values that are not equal to given value."""
  lastReload_not: DateTime

  """All values that are contained in given list."""
  lastReload_in: [DateTime!]

  """All values that are not contained in given list."""
  lastReload_not_in: [DateTime!]

  """All values less than the given value."""
  lastReload_lt: DateTime

  """All values less than or equal the given value."""
  lastReload_lte: DateTime

  """All values greater than the given value."""
  lastReload_gt: DateTime

  """All values greater than or equal the given value."""
  lastReload_gte: DateTime
  gameresults_every: GameresultWhereInput
  gameresults_some: GameresultWhereInput
  gameresults_none: GameresultWhereInput
}

input BggInfoWhereUniqueInput {
  bggid: Int
  id: ID
}

scalar DateTime

type File implements Node {
  contentType: String!
  createdAt: DateTime!
  id: ID!
  name: String!
  secret: String!
  size: Int!
  updatedAt: DateTime!
  url: String!
}

"""A connection to a list of items."""
type FileConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [FileEdge]!
  aggregate: AggregateFile!
}

input FileCreateInput {
  contentType: String!
  id: ID
  name: String!
  secret: String!
  size: Int!
  url: String!
}

"""An edge in a connection."""
type FileEdge {
  """The item at the end of the edge."""
  node: File!

  """A cursor for use in pagination."""
  cursor: String!
}

enum FileOrderByInput {
  contentType_ASC
  contentType_DESC
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  secret_ASC
  secret_DESC
  size_ASC
  size_DESC
  updatedAt_ASC
  updatedAt_DESC
  url_ASC
  url_DESC
}

type FilePreviousValues {
  contentType: String!
  createdAt: DateTime!
  id: ID!
  name: String!
  secret: String!
  size: Int!
  updatedAt: DateTime!
  url: String!
}

type FileSubscriptionPayload {
  mutation: MutationType!
  node: File
  updatedFields: [String!]
  previousValues: FilePreviousValues
}

input FileSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [FileSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [FileSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [FileSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: FileWhereInput
}

input FileUpdateInput {
  contentType: String
  name: String
  secret: String
  size: Int
  url: String
}

input FileUpdateManyMutationInput {
  contentType: String
  name: String
  secret: String
  size: Int
  url: String
}

input FileWhereInput {
  """Logical AND on all given filters."""
  AND: [FileWhereInput!]

  """Logical OR on all given filters."""
  OR: [FileWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [FileWhereInput!]
  contentType: String

  """All values that are not equal to given value."""
  contentType_not: String

  """All values that are contained in given list."""
  contentType_in: [String!]

  """All values that are not contained in given list."""
  contentType_not_in: [String!]

  """All values less than the given value."""
  contentType_lt: String

  """All values less than or equal the given value."""
  contentType_lte: String

  """All values greater than the given value."""
  contentType_gt: String

  """All values greater than or equal the given value."""
  contentType_gte: String

  """All values containing the given string."""
  contentType_contains: String

  """All values not containing the given string."""
  contentType_not_contains: String

  """All values starting with the given string."""
  contentType_starts_with: String

  """All values not starting with the given string."""
  contentType_not_starts_with: String

  """All values ending with the given string."""
  contentType_ends_with: String

  """All values not ending with the given string."""
  contentType_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  secret: String

  """All values that are not equal to given value."""
  secret_not: String

  """All values that are contained in given list."""
  secret_in: [String!]

  """All values that are not contained in given list."""
  secret_not_in: [String!]

  """All values less than the given value."""
  secret_lt: String

  """All values less than or equal the given value."""
  secret_lte: String

  """All values greater than the given value."""
  secret_gt: String

  """All values greater than or equal the given value."""
  secret_gte: String

  """All values containing the given string."""
  secret_contains: String

  """All values not containing the given string."""
  secret_not_contains: String

  """All values starting with the given string."""
  secret_starts_with: String

  """All values not starting with the given string."""
  secret_not_starts_with: String

  """All values ending with the given string."""
  secret_ends_with: String

  """All values not ending with the given string."""
  secret_not_ends_with: String
  size: Int

  """All values that are not equal to given value."""
  size_not: Int

  """All values that are contained in given list."""
  size_in: [Int!]

  """All values that are not contained in given list."""
  size_not_in: [Int!]

  """All values less than the given value."""
  size_lt: Int

  """All values less than or equal the given value."""
  size_lte: Int

  """All values greater than the given value."""
  size_gt: Int

  """All values greater than or equal the given value."""
  size_gte: Int
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  url: String

  """All values that are not equal to given value."""
  url_not: String

  """All values that are contained in given list."""
  url_in: [String!]

  """All values that are not contained in given list."""
  url_not_in: [String!]

  """All values less than the given value."""
  url_lt: String

  """All values less than or equal the given value."""
  url_lte: String

  """All values greater than the given value."""
  url_gt: String

  """All values greater than or equal the given value."""
  url_gte: String

  """All values containing the given string."""
  url_contains: String

  """All values not containing the given string."""
  url_not_contains: String

  """All values starting with the given string."""
  url_starts_with: String

  """All values not starting with the given string."""
  url_not_starts_with: String

  """All values ending with the given string."""
  url_ends_with: String

  """All values not ending with the given string."""
  url_not_ends_with: String
}

input FileWhereUniqueInput {
  id: ID
  secret: String
  url: String
}

type Gameresult implements Node {
  bggInfo: BggInfo
  createdAt: DateTime!
  id: ID!
  numPlayers: Int
  scores(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Score!]
  time: DateTime!
  tournament: Tournament!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type GameresultConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [GameresultEdge]!
  aggregate: AggregateGameresult!
}

input GameresultCreateInput {
  id: ID
  numPlayers: Int
  time: DateTime!
  bggInfo: BggInfoCreateOneWithoutGameresultsInput
  scores: ScoreCreateManyWithoutGameresultInput
  tournament: TournamentCreateOneWithoutGameresultsInput!
}

input GameresultCreateManyWithoutBggInfoInput {
  create: [GameresultCreateWithoutBggInfoInput!]
  connect: [GameresultWhereUniqueInput!]
}

input GameresultCreateManyWithoutTournamentInput {
  create: [GameresultCreateWithoutTournamentInput!]
  connect: [GameresultWhereUniqueInput!]
}

input GameresultCreateOneWithoutScoresInput {
  create: GameresultCreateWithoutScoresInput
  connect: GameresultWhereUniqueInput
}

input GameresultCreateWithoutBggInfoInput {
  id: ID
  numPlayers: Int
  time: DateTime!
  scores: ScoreCreateManyWithoutGameresultInput
  tournament: TournamentCreateOneWithoutGameresultsInput!
}

input GameresultCreateWithoutScoresInput {
  id: ID
  numPlayers: Int
  time: DateTime!
  bggInfo: BggInfoCreateOneWithoutGameresultsInput
  tournament: TournamentCreateOneWithoutGameresultsInput!
}

input GameresultCreateWithoutTournamentInput {
  id: ID
  numPlayers: Int
  time: DateTime!
  bggInfo: BggInfoCreateOneWithoutGameresultsInput
  scores: ScoreCreateManyWithoutGameresultInput
}

"""An edge in a connection."""
type GameresultEdge {
  """The item at the end of the edge."""
  node: Gameresult!

  """A cursor for use in pagination."""
  cursor: String!
}

enum GameresultOrderByInput {
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  numPlayers_ASC
  numPlayers_DESC
  time_ASC
  time_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type GameresultPreviousValues {
  createdAt: DateTime!
  id: ID!
  numPlayers: Int
  time: DateTime!
  updatedAt: DateTime!
}

input GameresultScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [GameresultScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [GameresultScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GameresultScalarWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  numPlayers: Int

  """All values that are not equal to given value."""
  numPlayers_not: Int

  """All values that are contained in given list."""
  numPlayers_in: [Int!]

  """All values that are not contained in given list."""
  numPlayers_not_in: [Int!]

  """All values less than the given value."""
  numPlayers_lt: Int

  """All values less than or equal the given value."""
  numPlayers_lte: Int

  """All values greater than the given value."""
  numPlayers_gt: Int

  """All values greater than or equal the given value."""
  numPlayers_gte: Int
  time: DateTime

  """All values that are not equal to given value."""
  time_not: DateTime

  """All values that are contained in given list."""
  time_in: [DateTime!]

  """All values that are not contained in given list."""
  time_not_in: [DateTime!]

  """All values less than the given value."""
  time_lt: DateTime

  """All values less than or equal the given value."""
  time_lte: DateTime

  """All values greater than the given value."""
  time_gt: DateTime

  """All values greater than or equal the given value."""
  time_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type GameresultSubscriptionPayload {
  mutation: MutationType!
  node: Gameresult
  updatedFields: [String!]
  previousValues: GameresultPreviousValues
}

input GameresultSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [GameresultSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [GameresultSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GameresultSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: GameresultWhereInput
}

input GameresultUpdateInput {
  numPlayers: Int
  time: DateTime
  bggInfo: BggInfoUpdateOneWithoutGameresultsInput
  scores: ScoreUpdateManyWithoutGameresultInput
  tournament: TournamentUpdateOneRequiredWithoutGameresultsInput
}

input GameresultUpdateManyDataInput {
  numPlayers: Int
  time: DateTime
}

input GameresultUpdateManyMutationInput {
  numPlayers: Int
  time: DateTime
}

input GameresultUpdateManyWithoutBggInfoInput {
  create: [GameresultCreateWithoutBggInfoInput!]
  connect: [GameresultWhereUniqueInput!]
  set: [GameresultWhereUniqueInput!]
  disconnect: [GameresultWhereUniqueInput!]
  delete: [GameresultWhereUniqueInput!]
  update: [GameresultUpdateWithWhereUniqueWithoutBggInfoInput!]
  updateMany: [GameresultUpdateManyWithWhereNestedInput!]
  deleteMany: [GameresultScalarWhereInput!]
  upsert: [GameresultUpsertWithWhereUniqueWithoutBggInfoInput!]
}

input GameresultUpdateManyWithoutTournamentInput {
  create: [GameresultCreateWithoutTournamentInput!]
  connect: [GameresultWhereUniqueInput!]
  set: [GameresultWhereUniqueInput!]
  disconnect: [GameresultWhereUniqueInput!]
  delete: [GameresultWhereUniqueInput!]
  update: [GameresultUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [GameresultUpdateManyWithWhereNestedInput!]
  deleteMany: [GameresultScalarWhereInput!]
  upsert: [GameresultUpsertWithWhereUniqueWithoutTournamentInput!]
}

input GameresultUpdateManyWithWhereNestedInput {
  where: GameresultScalarWhereInput!
  data: GameresultUpdateManyDataInput!
}

input GameresultUpdateOneWithoutScoresInput {
  create: GameresultCreateWithoutScoresInput
  connect: GameresultWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: GameresultUpdateWithoutScoresDataInput
  upsert: GameresultUpsertWithoutScoresInput
}

input GameresultUpdateWithoutBggInfoDataInput {
  numPlayers: Int
  time: DateTime
  scores: ScoreUpdateManyWithoutGameresultInput
  tournament: TournamentUpdateOneRequiredWithoutGameresultsInput
}

input GameresultUpdateWithoutScoresDataInput {
  numPlayers: Int
  time: DateTime
  bggInfo: BggInfoUpdateOneWithoutGameresultsInput
  tournament: TournamentUpdateOneRequiredWithoutGameresultsInput
}

input GameresultUpdateWithoutTournamentDataInput {
  numPlayers: Int
  time: DateTime
  bggInfo: BggInfoUpdateOneWithoutGameresultsInput
  scores: ScoreUpdateManyWithoutGameresultInput
}

input GameresultUpdateWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput!
  data: GameresultUpdateWithoutBggInfoDataInput!
}

input GameresultUpdateWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput!
  data: GameresultUpdateWithoutTournamentDataInput!
}

input GameresultUpsertWithoutScoresInput {
  update: GameresultUpdateWithoutScoresDataInput!
  create: GameresultCreateWithoutScoresInput!
}

input GameresultUpsertWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput!
  update: GameresultUpdateWithoutBggInfoDataInput!
  create: GameresultCreateWithoutBggInfoInput!
}

input GameresultUpsertWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput!
  update: GameresultUpdateWithoutTournamentDataInput!
  create: GameresultCreateWithoutTournamentInput!
}

input GameresultWhereInput {
  """Logical AND on all given filters."""
  AND: [GameresultWhereInput!]

  """Logical OR on all given filters."""
  OR: [GameresultWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GameresultWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  numPlayers: Int

  """All values that are not equal to given value."""
  numPlayers_not: Int

  """All values that are contained in given list."""
  numPlayers_in: [Int!]

  """All values that are not contained in given list."""
  numPlayers_not_in: [Int!]

  """All values less than the given value."""
  numPlayers_lt: Int

  """All values less than or equal the given value."""
  numPlayers_lte: Int

  """All values greater than the given value."""
  numPlayers_gt: Int

  """All values greater than or equal the given value."""
  numPlayers_gte: Int
  time: DateTime

  """All values that are not equal to given value."""
  time_not: DateTime

  """All values that are contained in given list."""
  time_in: [DateTime!]

  """All values that are not contained in given list."""
  time_not_in: [DateTime!]

  """All values less than the given value."""
  time_lt: DateTime

  """All values less than or equal the given value."""
  time_lte: DateTime

  """All values greater than the given value."""
  time_gt: DateTime

  """All values greater than or equal the given value."""
  time_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  bggInfo: BggInfoWhereInput
  scores_every: ScoreWhereInput
  scores_some: ScoreWhereInput
  scores_none: ScoreWhereInput
  tournament: TournamentWhereInput
}

input GameresultWhereUniqueInput {
  id: ID
}

type Invitation implements Node {
  id: ID!
  token: String!
  state: InvitationState!
  email: String!
  inviter: User!
  tournament: Tournament!
  createdAt: DateTime!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type InvitationConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [InvitationEdge]!
  aggregate: AggregateInvitation!
}

input InvitationCreateInput {
  id: ID
  token: String!
  state: InvitationState
  email: String!
  inviter: UserCreateOneInput!
  tournament: TournamentCreateOneWithoutInvitationsInput!
}

input InvitationCreateManyWithoutTournamentInput {
  create: [InvitationCreateWithoutTournamentInput!]
  connect: [InvitationWhereUniqueInput!]
}

input InvitationCreateWithoutTournamentInput {
  id: ID
  token: String!
  state: InvitationState
  email: String!
  inviter: UserCreateOneInput!
}

"""An edge in a connection."""
type InvitationEdge {
  """The item at the end of the edge."""
  node: Invitation!

  """A cursor for use in pagination."""
  cursor: String!
}

enum InvitationOrderByInput {
  id_ASC
  id_DESC
  token_ASC
  token_DESC
  state_ASC
  state_DESC
  email_ASC
  email_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type InvitationPreviousValues {
  id: ID!
  token: String!
  state: InvitationState!
  email: String!
  createdAt: DateTime!
  updatedAt: DateTime!
}

input InvitationScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [InvitationScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [InvitationScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [InvitationScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  token: String

  """All values that are not equal to given value."""
  token_not: String

  """All values that are contained in given list."""
  token_in: [String!]

  """All values that are not contained in given list."""
  token_not_in: [String!]

  """All values less than the given value."""
  token_lt: String

  """All values less than or equal the given value."""
  token_lte: String

  """All values greater than the given value."""
  token_gt: String

  """All values greater than or equal the given value."""
  token_gte: String

  """All values containing the given string."""
  token_contains: String

  """All values not containing the given string."""
  token_not_contains: String

  """All values starting with the given string."""
  token_starts_with: String

  """All values not starting with the given string."""
  token_not_starts_with: String

  """All values ending with the given string."""
  token_ends_with: String

  """All values not ending with the given string."""
  token_not_ends_with: String
  state: InvitationState

  """All values that are not equal to given value."""
  state_not: InvitationState

  """All values that are contained in given list."""
  state_in: [InvitationState!]

  """All values that are not contained in given list."""
  state_not_in: [InvitationState!]
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

enum InvitationState {
  PENDING
  ACCEPTED
  REJECTED
}

type InvitationSubscriptionPayload {
  mutation: MutationType!
  node: Invitation
  updatedFields: [String!]
  previousValues: InvitationPreviousValues
}

input InvitationSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [InvitationSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [InvitationSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [InvitationSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: InvitationWhereInput
}

input InvitationUpdateInput {
  token: String
  state: InvitationState
  email: String
  inviter: UserUpdateOneRequiredInput
  tournament: TournamentUpdateOneRequiredWithoutInvitationsInput
}

input InvitationUpdateManyDataInput {
  token: String
  state: InvitationState
  email: String
}

input InvitationUpdateManyMutationInput {
  token: String
  state: InvitationState
  email: String
}

input InvitationUpdateManyWithoutTournamentInput {
  create: [InvitationCreateWithoutTournamentInput!]
  connect: [InvitationWhereUniqueInput!]
  set: [InvitationWhereUniqueInput!]
  disconnect: [InvitationWhereUniqueInput!]
  delete: [InvitationWhereUniqueInput!]
  update: [InvitationUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [InvitationUpdateManyWithWhereNestedInput!]
  deleteMany: [InvitationScalarWhereInput!]
  upsert: [InvitationUpsertWithWhereUniqueWithoutTournamentInput!]
}

input InvitationUpdateManyWithWhereNestedInput {
  where: InvitationScalarWhereInput!
  data: InvitationUpdateManyDataInput!
}

input InvitationUpdateWithoutTournamentDataInput {
  token: String
  state: InvitationState
  email: String
  inviter: UserUpdateOneRequiredInput
}

input InvitationUpdateWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput!
  data: InvitationUpdateWithoutTournamentDataInput!
}

input InvitationUpsertWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput!
  update: InvitationUpdateWithoutTournamentDataInput!
  create: InvitationCreateWithoutTournamentInput!
}

input InvitationWhereInput {
  """Logical AND on all given filters."""
  AND: [InvitationWhereInput!]

  """Logical OR on all given filters."""
  OR: [InvitationWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [InvitationWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  token: String

  """All values that are not equal to given value."""
  token_not: String

  """All values that are contained in given list."""
  token_in: [String!]

  """All values that are not contained in given list."""
  token_not_in: [String!]

  """All values less than the given value."""
  token_lt: String

  """All values less than or equal the given value."""
  token_lte: String

  """All values greater than the given value."""
  token_gt: String

  """All values greater than or equal the given value."""
  token_gte: String

  """All values containing the given string."""
  token_contains: String

  """All values not containing the given string."""
  token_not_contains: String

  """All values starting with the given string."""
  token_starts_with: String

  """All values not starting with the given string."""
  token_not_starts_with: String

  """All values ending with the given string."""
  token_ends_with: String

  """All values not ending with the given string."""
  token_not_ends_with: String
  state: InvitationState

  """All values that are not equal to given value."""
  state_not: InvitationState

  """All values that are contained in given list."""
  state_in: [InvitationState!]

  """All values that are not contained in given list."""
  state_not_in: [InvitationState!]
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  inviter: UserWhereInput
  tournament: TournamentWhereInput
}

input InvitationWhereUniqueInput {
  id: ID
  token: String
}

"""
The \`Long\` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
"""
scalar Long

type Mutation {
  createTournamentMember(data: TournamentMemberCreateInput!): TournamentMember!
  createBggInfo(data: BggInfoCreateInput!): BggInfo!
  createFile(data: FileCreateInput!): File!
  createGameresult(data: GameresultCreateInput!): Gameresult!
  createPlayer(data: PlayerCreateInput!): Player!
  createScore(data: ScoreCreateInput!): Score!
  createTournament(data: TournamentCreateInput!): Tournament!
  createInvitation(data: InvitationCreateInput!): Invitation!
  createUser(data: UserCreateInput!): User!
  updateTournamentMember(data: TournamentMemberUpdateInput!, where: TournamentMemberWhereUniqueInput!): TournamentMember
  updateBggInfo(data: BggInfoUpdateInput!, where: BggInfoWhereUniqueInput!): BggInfo
  updateFile(data: FileUpdateInput!, where: FileWhereUniqueInput!): File
  updateGameresult(data: GameresultUpdateInput!, where: GameresultWhereUniqueInput!): Gameresult
  updatePlayer(data: PlayerUpdateInput!, where: PlayerWhereUniqueInput!): Player
  updateScore(data: ScoreUpdateInput!, where: ScoreWhereUniqueInput!): Score
  updateTournament(data: TournamentUpdateInput!, where: TournamentWhereUniqueInput!): Tournament
  updateInvitation(data: InvitationUpdateInput!, where: InvitationWhereUniqueInput!): Invitation
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  deleteTournamentMember(where: TournamentMemberWhereUniqueInput!): TournamentMember
  deleteBggInfo(where: BggInfoWhereUniqueInput!): BggInfo
  deleteFile(where: FileWhereUniqueInput!): File
  deleteGameresult(where: GameresultWhereUniqueInput!): Gameresult
  deletePlayer(where: PlayerWhereUniqueInput!): Player
  deleteScore(where: ScoreWhereUniqueInput!): Score
  deleteTournament(where: TournamentWhereUniqueInput!): Tournament
  deleteInvitation(where: InvitationWhereUniqueInput!): Invitation
  deleteUser(where: UserWhereUniqueInput!): User
  upsertTournamentMember(where: TournamentMemberWhereUniqueInput!, create: TournamentMemberCreateInput!, update: TournamentMemberUpdateInput!): TournamentMember!
  upsertBggInfo(where: BggInfoWhereUniqueInput!, create: BggInfoCreateInput!, update: BggInfoUpdateInput!): BggInfo!
  upsertFile(where: FileWhereUniqueInput!, create: FileCreateInput!, update: FileUpdateInput!): File!
  upsertGameresult(where: GameresultWhereUniqueInput!, create: GameresultCreateInput!, update: GameresultUpdateInput!): Gameresult!
  upsertPlayer(where: PlayerWhereUniqueInput!, create: PlayerCreateInput!, update: PlayerUpdateInput!): Player!
  upsertScore(where: ScoreWhereUniqueInput!, create: ScoreCreateInput!, update: ScoreUpdateInput!): Score!
  upsertTournament(where: TournamentWhereUniqueInput!, create: TournamentCreateInput!, update: TournamentUpdateInput!): Tournament!
  upsertInvitation(where: InvitationWhereUniqueInput!, create: InvitationCreateInput!, update: InvitationUpdateInput!): Invitation!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  updateManyTournamentMembers(data: TournamentMemberUpdateManyMutationInput!, where: TournamentMemberWhereInput): BatchPayload!
  updateManyBggInfoes(data: BggInfoUpdateManyMutationInput!, where: BggInfoWhereInput): BatchPayload!
  updateManyFiles(data: FileUpdateManyMutationInput!, where: FileWhereInput): BatchPayload!
  updateManyGameresults(data: GameresultUpdateManyMutationInput!, where: GameresultWhereInput): BatchPayload!
  updateManyPlayers(data: PlayerUpdateManyMutationInput!, where: PlayerWhereInput): BatchPayload!
  updateManyScores(data: ScoreUpdateManyMutationInput!, where: ScoreWhereInput): BatchPayload!
  updateManyTournaments(data: TournamentUpdateManyMutationInput!, where: TournamentWhereInput): BatchPayload!
  updateManyInvitations(data: InvitationUpdateManyMutationInput!, where: InvitationWhereInput): BatchPayload!
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  deleteManyTournamentMembers(where: TournamentMemberWhereInput): BatchPayload!
  deleteManyBggInfoes(where: BggInfoWhereInput): BatchPayload!
  deleteManyFiles(where: FileWhereInput): BatchPayload!
  deleteManyGameresults(where: GameresultWhereInput): BatchPayload!
  deleteManyPlayers(where: PlayerWhereInput): BatchPayload!
  deleteManyScores(where: ScoreWhereInput): BatchPayload!
  deleteManyTournaments(where: TournamentWhereInput): BatchPayload!
  deleteManyInvitations(where: InvitationWhereInput): BatchPayload!
  deleteManyUsers(where: UserWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

type Player implements Node {
  createdAt: DateTime!
  id: ID!
  name: String!
  scores(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Score!]
  tournament: Tournament!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type PlayerConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PlayerEdge]!
  aggregate: AggregatePlayer!
}

input PlayerCreateInput {
  id: ID
  name: String!
  scores: ScoreCreateManyWithoutPlayerInput
  tournament: TournamentCreateOneWithoutPlayersInput!
}

input PlayerCreateManyWithoutTournamentInput {
  create: [PlayerCreateWithoutTournamentInput!]
  connect: [PlayerWhereUniqueInput!]
}

input PlayerCreateOneWithoutScoresInput {
  create: PlayerCreateWithoutScoresInput
  connect: PlayerWhereUniqueInput
}

input PlayerCreateWithoutScoresInput {
  id: ID
  name: String!
  tournament: TournamentCreateOneWithoutPlayersInput!
}

input PlayerCreateWithoutTournamentInput {
  id: ID
  name: String!
  scores: ScoreCreateManyWithoutPlayerInput
}

"""An edge in a connection."""
type PlayerEdge {
  """The item at the end of the edge."""
  node: Player!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PlayerOrderByInput {
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type PlayerPreviousValues {
  createdAt: DateTime!
  id: ID!
  name: String!
  updatedAt: DateTime!
}

input PlayerScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [PlayerScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [PlayerScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PlayerScalarWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type PlayerSubscriptionPayload {
  mutation: MutationType!
  node: Player
  updatedFields: [String!]
  previousValues: PlayerPreviousValues
}

input PlayerSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PlayerSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PlayerSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PlayerSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PlayerWhereInput
}

input PlayerUpdateInput {
  name: String
  scores: ScoreUpdateManyWithoutPlayerInput
  tournament: TournamentUpdateOneRequiredWithoutPlayersInput
}

input PlayerUpdateManyDataInput {
  name: String
}

input PlayerUpdateManyMutationInput {
  name: String
}

input PlayerUpdateManyWithoutTournamentInput {
  create: [PlayerCreateWithoutTournamentInput!]
  connect: [PlayerWhereUniqueInput!]
  set: [PlayerWhereUniqueInput!]
  disconnect: [PlayerWhereUniqueInput!]
  delete: [PlayerWhereUniqueInput!]
  update: [PlayerUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [PlayerUpdateManyWithWhereNestedInput!]
  deleteMany: [PlayerScalarWhereInput!]
  upsert: [PlayerUpsertWithWhereUniqueWithoutTournamentInput!]
}

input PlayerUpdateManyWithWhereNestedInput {
  where: PlayerScalarWhereInput!
  data: PlayerUpdateManyDataInput!
}

input PlayerUpdateOneRequiredWithoutScoresInput {
  create: PlayerCreateWithoutScoresInput
  connect: PlayerWhereUniqueInput
  update: PlayerUpdateWithoutScoresDataInput
  upsert: PlayerUpsertWithoutScoresInput
}

input PlayerUpdateWithoutScoresDataInput {
  name: String
  tournament: TournamentUpdateOneRequiredWithoutPlayersInput
}

input PlayerUpdateWithoutTournamentDataInput {
  name: String
  scores: ScoreUpdateManyWithoutPlayerInput
}

input PlayerUpdateWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput!
  data: PlayerUpdateWithoutTournamentDataInput!
}

input PlayerUpsertWithoutScoresInput {
  update: PlayerUpdateWithoutScoresDataInput!
  create: PlayerCreateWithoutScoresInput!
}

input PlayerUpsertWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput!
  update: PlayerUpdateWithoutTournamentDataInput!
  create: PlayerCreateWithoutTournamentInput!
}

input PlayerWhereInput {
  """Logical AND on all given filters."""
  AND: [PlayerWhereInput!]

  """Logical OR on all given filters."""
  OR: [PlayerWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PlayerWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  scores_every: ScoreWhereInput
  scores_some: ScoreWhereInput
  scores_none: ScoreWhereInput
  tournament: TournamentWhereInput
}

input PlayerWhereUniqueInput {
  id: ID
}

type Query {
  tournamentMembers(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TournamentMember]!
  bggInfoes(where: BggInfoWhereInput, orderBy: BggInfoOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [BggInfo]!
  files(where: FileWhereInput, orderBy: FileOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [File]!
  gameresults(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Gameresult]!
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player]!
  scores(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Score]!
  tournaments(where: TournamentWhereInput, orderBy: TournamentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Tournament]!
  invitations(where: InvitationWhereInput, orderBy: InvitationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Invitation]!
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  tournamentMember(where: TournamentMemberWhereUniqueInput!): TournamentMember
  bggInfo(where: BggInfoWhereUniqueInput!): BggInfo
  file(where: FileWhereUniqueInput!): File
  gameresult(where: GameresultWhereUniqueInput!): Gameresult
  player(where: PlayerWhereUniqueInput!): Player
  score(where: ScoreWhereUniqueInput!): Score
  tournament(where: TournamentWhereUniqueInput!): Tournament
  invitation(where: InvitationWhereUniqueInput!): Invitation
  user(where: UserWhereUniqueInput!): User
  tournamentMembersConnection(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TournamentMemberConnection!
  bggInfoesConnection(where: BggInfoWhereInput, orderBy: BggInfoOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): BggInfoConnection!
  filesConnection(where: FileWhereInput, orderBy: FileOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): FileConnection!
  gameresultsConnection(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): GameresultConnection!
  playersConnection(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PlayerConnection!
  scoresConnection(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ScoreConnection!
  tournamentsConnection(where: TournamentWhereInput, orderBy: TournamentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TournamentConnection!
  invitationsConnection(where: InvitationWhereInput, orderBy: InvitationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): InvitationConnection!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

type Score implements Node {
  createdAt: DateTime!
  gameresult: Gameresult
  id: ID!
  player: Player!
  score: Int!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type ScoreConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [ScoreEdge]!
  aggregate: AggregateScore!
}

input ScoreCreateInput {
  id: ID
  score: Int!
  gameresult: GameresultCreateOneWithoutScoresInput
  player: PlayerCreateOneWithoutScoresInput!
}

input ScoreCreateManyWithoutGameresultInput {
  create: [ScoreCreateWithoutGameresultInput!]
  connect: [ScoreWhereUniqueInput!]
}

input ScoreCreateManyWithoutPlayerInput {
  create: [ScoreCreateWithoutPlayerInput!]
  connect: [ScoreWhereUniqueInput!]
}

input ScoreCreateWithoutGameresultInput {
  id: ID
  score: Int!
  player: PlayerCreateOneWithoutScoresInput!
}

input ScoreCreateWithoutPlayerInput {
  id: ID
  score: Int!
  gameresult: GameresultCreateOneWithoutScoresInput
}

"""An edge in a connection."""
type ScoreEdge {
  """The item at the end of the edge."""
  node: Score!

  """A cursor for use in pagination."""
  cursor: String!
}

enum ScoreOrderByInput {
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  score_ASC
  score_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type ScorePreviousValues {
  createdAt: DateTime!
  id: ID!
  score: Int!
  updatedAt: DateTime!
}

input ScoreScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [ScoreScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [ScoreScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ScoreScalarWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  score: Int

  """All values that are not equal to given value."""
  score_not: Int

  """All values that are contained in given list."""
  score_in: [Int!]

  """All values that are not contained in given list."""
  score_not_in: [Int!]

  """All values less than the given value."""
  score_lt: Int

  """All values less than or equal the given value."""
  score_lte: Int

  """All values greater than the given value."""
  score_gt: Int

  """All values greater than or equal the given value."""
  score_gte: Int
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type ScoreSubscriptionPayload {
  mutation: MutationType!
  node: Score
  updatedFields: [String!]
  previousValues: ScorePreviousValues
}

input ScoreSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [ScoreSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [ScoreSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ScoreSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: ScoreWhereInput
}

input ScoreUpdateInput {
  score: Int
  gameresult: GameresultUpdateOneWithoutScoresInput
  player: PlayerUpdateOneRequiredWithoutScoresInput
}

input ScoreUpdateManyDataInput {
  score: Int
}

input ScoreUpdateManyMutationInput {
  score: Int
}

input ScoreUpdateManyWithoutGameresultInput {
  create: [ScoreCreateWithoutGameresultInput!]
  connect: [ScoreWhereUniqueInput!]
  set: [ScoreWhereUniqueInput!]
  disconnect: [ScoreWhereUniqueInput!]
  delete: [ScoreWhereUniqueInput!]
  update: [ScoreUpdateWithWhereUniqueWithoutGameresultInput!]
  updateMany: [ScoreUpdateManyWithWhereNestedInput!]
  deleteMany: [ScoreScalarWhereInput!]
  upsert: [ScoreUpsertWithWhereUniqueWithoutGameresultInput!]
}

input ScoreUpdateManyWithoutPlayerInput {
  create: [ScoreCreateWithoutPlayerInput!]
  connect: [ScoreWhereUniqueInput!]
  set: [ScoreWhereUniqueInput!]
  disconnect: [ScoreWhereUniqueInput!]
  delete: [ScoreWhereUniqueInput!]
  update: [ScoreUpdateWithWhereUniqueWithoutPlayerInput!]
  updateMany: [ScoreUpdateManyWithWhereNestedInput!]
  deleteMany: [ScoreScalarWhereInput!]
  upsert: [ScoreUpsertWithWhereUniqueWithoutPlayerInput!]
}

input ScoreUpdateManyWithWhereNestedInput {
  where: ScoreScalarWhereInput!
  data: ScoreUpdateManyDataInput!
}

input ScoreUpdateWithoutGameresultDataInput {
  score: Int
  player: PlayerUpdateOneRequiredWithoutScoresInput
}

input ScoreUpdateWithoutPlayerDataInput {
  score: Int
  gameresult: GameresultUpdateOneWithoutScoresInput
}

input ScoreUpdateWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput!
  data: ScoreUpdateWithoutGameresultDataInput!
}

input ScoreUpdateWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput!
  data: ScoreUpdateWithoutPlayerDataInput!
}

input ScoreUpsertWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput!
  update: ScoreUpdateWithoutGameresultDataInput!
  create: ScoreCreateWithoutGameresultInput!
}

input ScoreUpsertWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput!
  update: ScoreUpdateWithoutPlayerDataInput!
  create: ScoreCreateWithoutPlayerInput!
}

input ScoreWhereInput {
  """Logical AND on all given filters."""
  AND: [ScoreWhereInput!]

  """Logical OR on all given filters."""
  OR: [ScoreWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ScoreWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  score: Int

  """All values that are not equal to given value."""
  score_not: Int

  """All values that are contained in given list."""
  score_in: [Int!]

  """All values that are not contained in given list."""
  score_not_in: [Int!]

  """All values less than the given value."""
  score_lt: Int

  """All values less than or equal the given value."""
  score_lte: Int

  """All values greater than the given value."""
  score_gt: Int

  """All values greater than or equal the given value."""
  score_gte: Int
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  gameresult: GameresultWhereInput
  player: PlayerWhereInput
}

input ScoreWhereUniqueInput {
  id: ID
}

type Subscription {
  tournamentMember(where: TournamentMemberSubscriptionWhereInput): TournamentMemberSubscriptionPayload
  bggInfo(where: BggInfoSubscriptionWhereInput): BggInfoSubscriptionPayload
  file(where: FileSubscriptionWhereInput): FileSubscriptionPayload
  gameresult(where: GameresultSubscriptionWhereInput): GameresultSubscriptionPayload
  player(where: PlayerSubscriptionWhereInput): PlayerSubscriptionPayload
  score(where: ScoreSubscriptionWhereInput): ScoreSubscriptionPayload
  tournament(where: TournamentSubscriptionWhereInput): TournamentSubscriptionPayload
  invitation(where: InvitationSubscriptionWhereInput): InvitationSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
}

type Tournament implements Node {
  id: ID!
  slug: String!
  gameresults(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Gameresult!]
  name: String!
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player!]
  members(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TournamentMember!]
  invitations(where: InvitationWhereInput, orderBy: InvitationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Invitation!]
  createdAt: DateTime!
  updatedAt: DateTime!
  lastGameresultAt: DateTime
  numResults: Int!
  numPlayers: Int!
  numGames: Int!
}

"""A connection to a list of items."""
type TournamentConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TournamentEdge]!
  aggregate: AggregateTournament!
}

input TournamentCreateInput {
  id: ID
  slug: String!
  name: String!
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

input TournamentCreateOneWithoutGameresultsInput {
  create: TournamentCreateWithoutGameresultsInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateOneWithoutInvitationsInput {
  create: TournamentCreateWithoutInvitationsInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateOneWithoutMembersInput {
  create: TournamentCreateWithoutMembersInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateOneWithoutPlayersInput {
  create: TournamentCreateWithoutPlayersInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateWithoutGameresultsInput {
  id: ID
  slug: String!
  name: String!
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

input TournamentCreateWithoutInvitationsInput {
  id: ID
  slug: String!
  name: String!
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
}

input TournamentCreateWithoutMembersInput {
  id: ID
  slug: String!
  name: String!
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

input TournamentCreateWithoutPlayersInput {
  id: ID
  slug: String!
  name: String!
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

"""An edge in a connection."""
type TournamentEdge {
  """The item at the end of the edge."""
  node: Tournament!

  """A cursor for use in pagination."""
  cursor: String!
}

type TournamentMember implements Node {
  id: ID!
  tournament: Tournament!
  user: User!
  role: TournamentMemberRole!
  createdAt: DateTime!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type TournamentMemberConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TournamentMemberEdge]!
  aggregate: AggregateTournamentMember!
}

input TournamentMemberCreateInput {
  id: ID
  role: TournamentMemberRole
  tournament: TournamentCreateOneWithoutMembersInput!
  user: UserCreateOneWithoutTournamentsInput!
}

input TournamentMemberCreateManyWithoutTournamentInput {
  create: [TournamentMemberCreateWithoutTournamentInput!]
  connect: [TournamentMemberWhereUniqueInput!]
}

input TournamentMemberCreateManyWithoutUserInput {
  create: [TournamentMemberCreateWithoutUserInput!]
  connect: [TournamentMemberWhereUniqueInput!]
}

input TournamentMemberCreateWithoutTournamentInput {
  id: ID
  role: TournamentMemberRole
  user: UserCreateOneWithoutTournamentsInput!
}

input TournamentMemberCreateWithoutUserInput {
  id: ID
  role: TournamentMemberRole
  tournament: TournamentCreateOneWithoutMembersInput!
}

"""An edge in a connection."""
type TournamentMemberEdge {
  """The item at the end of the edge."""
  node: TournamentMember!

  """A cursor for use in pagination."""
  cursor: String!
}

enum TournamentMemberOrderByInput {
  id_ASC
  id_DESC
  role_ASC
  role_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type TournamentMemberPreviousValues {
  id: ID!
  role: TournamentMemberRole!
  createdAt: DateTime!
  updatedAt: DateTime!
}

enum TournamentMemberRole {
  ADMIN
  MEMBER
}

input TournamentMemberScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentMemberScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentMemberScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentMemberScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  role: TournamentMemberRole

  """All values that are not equal to given value."""
  role_not: TournamentMemberRole

  """All values that are contained in given list."""
  role_in: [TournamentMemberRole!]

  """All values that are not contained in given list."""
  role_not_in: [TournamentMemberRole!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type TournamentMemberSubscriptionPayload {
  mutation: MutationType!
  node: TournamentMember
  updatedFields: [String!]
  previousValues: TournamentMemberPreviousValues
}

input TournamentMemberSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentMemberSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentMemberSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentMemberSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TournamentMemberWhereInput
}

input TournamentMemberUpdateInput {
  role: TournamentMemberRole
  tournament: TournamentUpdateOneRequiredWithoutMembersInput
  user: UserUpdateOneRequiredWithoutTournamentsInput
}

input TournamentMemberUpdateManyDataInput {
  role: TournamentMemberRole
}

input TournamentMemberUpdateManyMutationInput {
  role: TournamentMemberRole
}

input TournamentMemberUpdateManyWithoutTournamentInput {
  create: [TournamentMemberCreateWithoutTournamentInput!]
  connect: [TournamentMemberWhereUniqueInput!]
  set: [TournamentMemberWhereUniqueInput!]
  disconnect: [TournamentMemberWhereUniqueInput!]
  delete: [TournamentMemberWhereUniqueInput!]
  update: [TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [TournamentMemberUpdateManyWithWhereNestedInput!]
  deleteMany: [TournamentMemberScalarWhereInput!]
  upsert: [TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput!]
}

input TournamentMemberUpdateManyWithoutUserInput {
  create: [TournamentMemberCreateWithoutUserInput!]
  connect: [TournamentMemberWhereUniqueInput!]
  set: [TournamentMemberWhereUniqueInput!]
  disconnect: [TournamentMemberWhereUniqueInput!]
  delete: [TournamentMemberWhereUniqueInput!]
  update: [TournamentMemberUpdateWithWhereUniqueWithoutUserInput!]
  updateMany: [TournamentMemberUpdateManyWithWhereNestedInput!]
  deleteMany: [TournamentMemberScalarWhereInput!]
  upsert: [TournamentMemberUpsertWithWhereUniqueWithoutUserInput!]
}

input TournamentMemberUpdateManyWithWhereNestedInput {
  where: TournamentMemberScalarWhereInput!
  data: TournamentMemberUpdateManyDataInput!
}

input TournamentMemberUpdateWithoutTournamentDataInput {
  role: TournamentMemberRole
  user: UserUpdateOneRequiredWithoutTournamentsInput
}

input TournamentMemberUpdateWithoutUserDataInput {
  role: TournamentMemberRole
  tournament: TournamentUpdateOneRequiredWithoutMembersInput
}

input TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput!
  data: TournamentMemberUpdateWithoutTournamentDataInput!
}

input TournamentMemberUpdateWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput!
  data: TournamentMemberUpdateWithoutUserDataInput!
}

input TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput!
  update: TournamentMemberUpdateWithoutTournamentDataInput!
  create: TournamentMemberCreateWithoutTournamentInput!
}

input TournamentMemberUpsertWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput!
  update: TournamentMemberUpdateWithoutUserDataInput!
  create: TournamentMemberCreateWithoutUserInput!
}

input TournamentMemberWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentMemberWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentMemberWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentMemberWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  role: TournamentMemberRole

  """All values that are not equal to given value."""
  role_not: TournamentMemberRole

  """All values that are contained in given list."""
  role_in: [TournamentMemberRole!]

  """All values that are not contained in given list."""
  role_not_in: [TournamentMemberRole!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  tournament: TournamentWhereInput
  user: UserWhereInput
}

input TournamentMemberWhereUniqueInput {
  id: ID
}

enum TournamentOrderByInput {
  id_ASC
  id_DESC
  slug_ASC
  slug_DESC
  name_ASC
  name_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
  lastGameresultAt_ASC
  lastGameresultAt_DESC
  numResults_ASC
  numResults_DESC
  numPlayers_ASC
  numPlayers_DESC
  numGames_ASC
  numGames_DESC
}

type TournamentPreviousValues {
  id: ID!
  slug: String!
  name: String!
  createdAt: DateTime!
  updatedAt: DateTime!
  lastGameresultAt: DateTime
  numResults: Int!
  numPlayers: Int!
  numGames: Int!
}

type TournamentSubscriptionPayload {
  mutation: MutationType!
  node: Tournament
  updatedFields: [String!]
  previousValues: TournamentPreviousValues
}

input TournamentSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TournamentWhereInput
}

input TournamentUpdateInput {
  slug: String
  name: String
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultUpdateManyWithoutTournamentInput
  players: PlayerUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpdateManyMutationInput {
  slug: String
  name: String
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
}

input TournamentUpdateOneRequiredWithoutGameresultsInput {
  create: TournamentCreateWithoutGameresultsInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutGameresultsDataInput
  upsert: TournamentUpsertWithoutGameresultsInput
}

input TournamentUpdateOneRequiredWithoutInvitationsInput {
  create: TournamentCreateWithoutInvitationsInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutInvitationsDataInput
  upsert: TournamentUpsertWithoutInvitationsInput
}

input TournamentUpdateOneRequiredWithoutMembersInput {
  create: TournamentCreateWithoutMembersInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutMembersDataInput
  upsert: TournamentUpsertWithoutMembersInput
}

input TournamentUpdateOneRequiredWithoutPlayersInput {
  create: TournamentCreateWithoutPlayersInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutPlayersDataInput
  upsert: TournamentUpsertWithoutPlayersInput
}

input TournamentUpdateWithoutGameresultsDataInput {
  slug: String
  name: String
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  players: PlayerUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpdateWithoutInvitationsDataInput {
  slug: String
  name: String
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultUpdateManyWithoutTournamentInput
  players: PlayerUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
}

input TournamentUpdateWithoutMembersDataInput {
  slug: String
  name: String
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultUpdateManyWithoutTournamentInput
  players: PlayerUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpdateWithoutPlayersDataInput {
  slug: String
  name: String
  lastGameresultAt: DateTime
  numResults: Int
  numPlayers: Int
  numGames: Int
  gameresults: GameresultUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpsertWithoutGameresultsInput {
  update: TournamentUpdateWithoutGameresultsDataInput!
  create: TournamentCreateWithoutGameresultsInput!
}

input TournamentUpsertWithoutInvitationsInput {
  update: TournamentUpdateWithoutInvitationsDataInput!
  create: TournamentCreateWithoutInvitationsInput!
}

input TournamentUpsertWithoutMembersInput {
  update: TournamentUpdateWithoutMembersDataInput!
  create: TournamentCreateWithoutMembersInput!
}

input TournamentUpsertWithoutPlayersInput {
  update: TournamentUpdateWithoutPlayersDataInput!
  create: TournamentCreateWithoutPlayersInput!
}

input TournamentWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  slug: String

  """All values that are not equal to given value."""
  slug_not: String

  """All values that are contained in given list."""
  slug_in: [String!]

  """All values that are not contained in given list."""
  slug_not_in: [String!]

  """All values less than the given value."""
  slug_lt: String

  """All values less than or equal the given value."""
  slug_lte: String

  """All values greater than the given value."""
  slug_gt: String

  """All values greater than or equal the given value."""
  slug_gte: String

  """All values containing the given string."""
  slug_contains: String

  """All values not containing the given string."""
  slug_not_contains: String

  """All values starting with the given string."""
  slug_starts_with: String

  """All values not starting with the given string."""
  slug_not_starts_with: String

  """All values ending with the given string."""
  slug_ends_with: String

  """All values not ending with the given string."""
  slug_not_ends_with: String
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  lastGameresultAt: DateTime

  """All values that are not equal to given value."""
  lastGameresultAt_not: DateTime

  """All values that are contained in given list."""
  lastGameresultAt_in: [DateTime!]

  """All values that are not contained in given list."""
  lastGameresultAt_not_in: [DateTime!]

  """All values less than the given value."""
  lastGameresultAt_lt: DateTime

  """All values less than or equal the given value."""
  lastGameresultAt_lte: DateTime

  """All values greater than the given value."""
  lastGameresultAt_gt: DateTime

  """All values greater than or equal the given value."""
  lastGameresultAt_gte: DateTime
  numResults: Int

  """All values that are not equal to given value."""
  numResults_not: Int

  """All values that are contained in given list."""
  numResults_in: [Int!]

  """All values that are not contained in given list."""
  numResults_not_in: [Int!]

  """All values less than the given value."""
  numResults_lt: Int

  """All values less than or equal the given value."""
  numResults_lte: Int

  """All values greater than the given value."""
  numResults_gt: Int

  """All values greater than or equal the given value."""
  numResults_gte: Int
  numPlayers: Int

  """All values that are not equal to given value."""
  numPlayers_not: Int

  """All values that are contained in given list."""
  numPlayers_in: [Int!]

  """All values that are not contained in given list."""
  numPlayers_not_in: [Int!]

  """All values less than the given value."""
  numPlayers_lt: Int

  """All values less than or equal the given value."""
  numPlayers_lte: Int

  """All values greater than the given value."""
  numPlayers_gt: Int

  """All values greater than or equal the given value."""
  numPlayers_gte: Int
  numGames: Int

  """All values that are not equal to given value."""
  numGames_not: Int

  """All values that are contained in given list."""
  numGames_in: [Int!]

  """All values that are not contained in given list."""
  numGames_not_in: [Int!]

  """All values less than the given value."""
  numGames_lt: Int

  """All values less than or equal the given value."""
  numGames_lte: Int

  """All values greater than the given value."""
  numGames_gt: Int

  """All values greater than or equal the given value."""
  numGames_gte: Int
  gameresults_every: GameresultWhereInput
  gameresults_some: GameresultWhereInput
  gameresults_none: GameresultWhereInput
  players_every: PlayerWhereInput
  players_some: PlayerWhereInput
  players_none: PlayerWhereInput
  members_every: TournamentMemberWhereInput
  members_some: TournamentMemberWhereInput
  members_none: TournamentMemberWhereInput
  invitations_every: InvitationWhereInput
  invitations_some: InvitationWhereInput
  invitations_none: InvitationWhereInput
}

input TournamentWhereUniqueInput {
  id: ID
  slug: String
}

type User implements Node {
  id: ID!
  auth0id: String!
  username: String!
  avatar: String!
  tournaments(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TournamentMember!]
  createdAt: DateTime!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type UserConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  id: ID
  auth0id: String!
  username: String!
  avatar: String!
  tournaments: TournamentMemberCreateManyWithoutUserInput
}

input UserCreateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutTournamentsInput {
  create: UserCreateWithoutTournamentsInput
  connect: UserWhereUniqueInput
}

input UserCreateWithoutTournamentsInput {
  id: ID
  auth0id: String!
  username: String!
  avatar: String!
}

"""An edge in a connection."""
type UserEdge {
  """The item at the end of the edge."""
  node: User!

  """A cursor for use in pagination."""
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  auth0id_ASC
  auth0id_DESC
  username_ASC
  username_DESC
  avatar_ASC
  avatar_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type UserPreviousValues {
  id: ID!
  auth0id: String!
  username: String!
  avatar: String!
  createdAt: DateTime!
  updatedAt: DateTime!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [UserSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: UserWhereInput
}

input UserUpdateDataInput {
  auth0id: String
  username: String
  avatar: String
  tournaments: TournamentMemberUpdateManyWithoutUserInput
}

input UserUpdateInput {
  auth0id: String
  username: String
  avatar: String
  tournaments: TournamentMemberUpdateManyWithoutUserInput
}

input UserUpdateManyMutationInput {
  auth0id: String
  username: String
  avatar: String
}

input UserUpdateOneRequiredInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
  update: UserUpdateDataInput
  upsert: UserUpsertNestedInput
}

input UserUpdateOneRequiredWithoutTournamentsInput {
  create: UserCreateWithoutTournamentsInput
  connect: UserWhereUniqueInput
  update: UserUpdateWithoutTournamentsDataInput
  upsert: UserUpsertWithoutTournamentsInput
}

input UserUpdateWithoutTournamentsDataInput {
  auth0id: String
  username: String
  avatar: String
}

input UserUpsertNestedInput {
  update: UserUpdateDataInput!
  create: UserCreateInput!
}

input UserUpsertWithoutTournamentsInput {
  update: UserUpdateWithoutTournamentsDataInput!
  create: UserCreateWithoutTournamentsInput!
}

input UserWhereInput {
  """Logical AND on all given filters."""
  AND: [UserWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  auth0id: String

  """All values that are not equal to given value."""
  auth0id_not: String

  """All values that are contained in given list."""
  auth0id_in: [String!]

  """All values that are not contained in given list."""
  auth0id_not_in: [String!]

  """All values less than the given value."""
  auth0id_lt: String

  """All values less than or equal the given value."""
  auth0id_lte: String

  """All values greater than the given value."""
  auth0id_gt: String

  """All values greater than or equal the given value."""
  auth0id_gte: String

  """All values containing the given string."""
  auth0id_contains: String

  """All values not containing the given string."""
  auth0id_not_contains: String

  """All values starting with the given string."""
  auth0id_starts_with: String

  """All values not starting with the given string."""
  auth0id_not_starts_with: String

  """All values ending with the given string."""
  auth0id_ends_with: String

  """All values not ending with the given string."""
  auth0id_not_ends_with: String
  username: String

  """All values that are not equal to given value."""
  username_not: String

  """All values that are contained in given list."""
  username_in: [String!]

  """All values that are not contained in given list."""
  username_not_in: [String!]

  """All values less than the given value."""
  username_lt: String

  """All values less than or equal the given value."""
  username_lte: String

  """All values greater than the given value."""
  username_gt: String

  """All values greater than or equal the given value."""
  username_gte: String

  """All values containing the given string."""
  username_contains: String

  """All values not containing the given string."""
  username_not_contains: String

  """All values starting with the given string."""
  username_starts_with: String

  """All values not starting with the given string."""
  username_not_starts_with: String

  """All values ending with the given string."""
  username_ends_with: String

  """All values not ending with the given string."""
  username_not_ends_with: String
  avatar: String

  """All values that are not equal to given value."""
  avatar_not: String

  """All values that are contained in given list."""
  avatar_in: [String!]

  """All values that are not contained in given list."""
  avatar_not_in: [String!]

  """All values less than the given value."""
  avatar_lt: String

  """All values less than or equal the given value."""
  avatar_lte: String

  """All values greater than the given value."""
  avatar_gt: String

  """All values greater than or equal the given value."""
  avatar_gte: String

  """All values containing the given string."""
  avatar_contains: String

  """All values not containing the given string."""
  avatar_not_contains: String

  """All values starting with the given string."""
  avatar_starts_with: String

  """All values not starting with the given string."""
  avatar_not_starts_with: String

  """All values ending with the given string."""
  avatar_ends_with: String

  """All values not ending with the given string."""
  avatar_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  tournaments_every: TournamentMemberWhereInput
  tournaments_some: TournamentMemberWhereInput
  tournaments_none: TournamentMemberWhereInput
}

input UserWhereUniqueInput {
  id: ID
  auth0id: String
}
`

export const Prisma = makePrismaBindingClass<BindingConstructor<Prisma>>({
  typeDefs
})

/**
 * Types
 */

export type BggInfoOrderByInput =
  | 'bggid_ASC'
  | 'bggid_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'description_ASC'
  | 'description_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'maxPlayers_ASC'
  | 'maxPlayers_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'ownedBy_ASC'
  | 'ownedBy_DESC'
  | 'playTime_ASC'
  | 'playTime_DESC'
  | 'rank_ASC'
  | 'rank_DESC'
  | 'rating_ASC'
  | 'rating_DESC'
  | 'thumbnail_ASC'
  | 'thumbnail_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'weight_ASC'
  | 'weight_DESC'
  | 'yearPublished_ASC'
  | 'yearPublished_DESC'
  | 'lastReload_ASC'
  | 'lastReload_DESC'

export type FileOrderByInput =
  | 'contentType_ASC'
  | 'contentType_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'secret_ASC'
  | 'secret_DESC'
  | 'size_ASC'
  | 'size_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'url_ASC'
  | 'url_DESC'

export type GameresultOrderByInput =
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'numPlayers_ASC'
  | 'numPlayers_DESC'
  | 'time_ASC'
  | 'time_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type InvitationOrderByInput =
  | 'id_ASC'
  | 'id_DESC'
  | 'token_ASC'
  | 'token_DESC'
  | 'state_ASC'
  | 'state_DESC'
  | 'email_ASC'
  | 'email_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type InvitationState = 'PENDING' | 'ACCEPTED' | 'REJECTED'

export type MutationType = 'CREATED' | 'UPDATED' | 'DELETED'

export type PlayerOrderByInput =
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type ScoreOrderByInput =
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'score_ASC'
  | 'score_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type TournamentMemberOrderByInput =
  | 'id_ASC'
  | 'id_DESC'
  | 'role_ASC'
  | 'role_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type TournamentMemberRole = 'ADMIN' | 'MEMBER'

export type TournamentOrderByInput =
  | 'id_ASC'
  | 'id_DESC'
  | 'slug_ASC'
  | 'slug_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'lastGameresultAt_ASC'
  | 'lastGameresultAt_DESC'
  | 'numResults_ASC'
  | 'numResults_DESC'
  | 'numPlayers_ASC'
  | 'numPlayers_DESC'
  | 'numGames_ASC'
  | 'numGames_DESC'

export type UserOrderByInput =
  | 'id_ASC'
  | 'id_DESC'
  | 'auth0id_ASC'
  | 'auth0id_DESC'
  | 'username_ASC'
  | 'username_DESC'
  | 'avatar_ASC'
  | 'avatar_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export interface BggInfoCreateInput {
  bggid: Int
  description: String
  id?: ID_Input | null
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload?: DateTime | null
  gameresults?: GameresultCreateManyWithoutBggInfoInput | null
}

export interface BggInfoCreateOneWithoutGameresultsInput {
  create?: BggInfoCreateWithoutGameresultsInput | null
  connect?: BggInfoWhereUniqueInput | null
}

export interface BggInfoCreateWithoutGameresultsInput {
  bggid: Int
  description: String
  id?: ID_Input | null
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload?: DateTime | null
}

export interface BggInfoSubscriptionWhereInput {
  AND?: BggInfoSubscriptionWhereInput[] | BggInfoSubscriptionWhereInput | null
  OR?: BggInfoSubscriptionWhereInput[] | BggInfoSubscriptionWhereInput | null
  NOT?: BggInfoSubscriptionWhereInput[] | BggInfoSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: BggInfoWhereInput | null
}

export interface BggInfoUpdateInput {
  bggid?: Int | null
  description?: String | null
  maxPlayers?: Int | null
  name?: String | null
  ownedBy?: Int | null
  playTime?: Int | null
  rank?: Int | null
  rating?: Float | null
  thumbnail?: String | null
  weight?: Float | null
  yearPublished?: Int | null
  lastReload?: DateTime | null
  gameresults?: GameresultUpdateManyWithoutBggInfoInput | null
}

export interface BggInfoUpdateManyMutationInput {
  bggid?: Int | null
  description?: String | null
  maxPlayers?: Int | null
  name?: String | null
  ownedBy?: Int | null
  playTime?: Int | null
  rank?: Int | null
  rating?: Float | null
  thumbnail?: String | null
  weight?: Float | null
  yearPublished?: Int | null
  lastReload?: DateTime | null
}

export interface BggInfoUpdateOneWithoutGameresultsInput {
  create?: BggInfoCreateWithoutGameresultsInput | null
  connect?: BggInfoWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: BggInfoUpdateWithoutGameresultsDataInput | null
  upsert?: BggInfoUpsertWithoutGameresultsInput | null
}

export interface BggInfoUpdateWithoutGameresultsDataInput {
  bggid?: Int | null
  description?: String | null
  maxPlayers?: Int | null
  name?: String | null
  ownedBy?: Int | null
  playTime?: Int | null
  rank?: Int | null
  rating?: Float | null
  thumbnail?: String | null
  weight?: Float | null
  yearPublished?: Int | null
  lastReload?: DateTime | null
}

export interface BggInfoUpsertWithoutGameresultsInput {
  update: BggInfoUpdateWithoutGameresultsDataInput
  create: BggInfoCreateWithoutGameresultsInput
}

export interface BggInfoWhereInput {
  AND?: BggInfoWhereInput[] | BggInfoWhereInput | null
  OR?: BggInfoWhereInput[] | BggInfoWhereInput | null
  NOT?: BggInfoWhereInput[] | BggInfoWhereInput | null
  bggid?: Int | null
  bggid_not?: Int | null
  bggid_in?: Int[] | Int | null
  bggid_not_in?: Int[] | Int | null
  bggid_lt?: Int | null
  bggid_lte?: Int | null
  bggid_gt?: Int | null
  bggid_gte?: Int | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  description?: String | null
  description_not?: String | null
  description_in?: String[] | String | null
  description_not_in?: String[] | String | null
  description_lt?: String | null
  description_lte?: String | null
  description_gt?: String | null
  description_gte?: String | null
  description_contains?: String | null
  description_not_contains?: String | null
  description_starts_with?: String | null
  description_not_starts_with?: String | null
  description_ends_with?: String | null
  description_not_ends_with?: String | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  maxPlayers?: Int | null
  maxPlayers_not?: Int | null
  maxPlayers_in?: Int[] | Int | null
  maxPlayers_not_in?: Int[] | Int | null
  maxPlayers_lt?: Int | null
  maxPlayers_lte?: Int | null
  maxPlayers_gt?: Int | null
  maxPlayers_gte?: Int | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  ownedBy?: Int | null
  ownedBy_not?: Int | null
  ownedBy_in?: Int[] | Int | null
  ownedBy_not_in?: Int[] | Int | null
  ownedBy_lt?: Int | null
  ownedBy_lte?: Int | null
  ownedBy_gt?: Int | null
  ownedBy_gte?: Int | null
  playTime?: Int | null
  playTime_not?: Int | null
  playTime_in?: Int[] | Int | null
  playTime_not_in?: Int[] | Int | null
  playTime_lt?: Int | null
  playTime_lte?: Int | null
  playTime_gt?: Int | null
  playTime_gte?: Int | null
  rank?: Int | null
  rank_not?: Int | null
  rank_in?: Int[] | Int | null
  rank_not_in?: Int[] | Int | null
  rank_lt?: Int | null
  rank_lte?: Int | null
  rank_gt?: Int | null
  rank_gte?: Int | null
  rating?: Float | null
  rating_not?: Float | null
  rating_in?: Float[] | Float | null
  rating_not_in?: Float[] | Float | null
  rating_lt?: Float | null
  rating_lte?: Float | null
  rating_gt?: Float | null
  rating_gte?: Float | null
  thumbnail?: String | null
  thumbnail_not?: String | null
  thumbnail_in?: String[] | String | null
  thumbnail_not_in?: String[] | String | null
  thumbnail_lt?: String | null
  thumbnail_lte?: String | null
  thumbnail_gt?: String | null
  thumbnail_gte?: String | null
  thumbnail_contains?: String | null
  thumbnail_not_contains?: String | null
  thumbnail_starts_with?: String | null
  thumbnail_not_starts_with?: String | null
  thumbnail_ends_with?: String | null
  thumbnail_not_ends_with?: String | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  weight?: Float | null
  weight_not?: Float | null
  weight_in?: Float[] | Float | null
  weight_not_in?: Float[] | Float | null
  weight_lt?: Float | null
  weight_lte?: Float | null
  weight_gt?: Float | null
  weight_gte?: Float | null
  yearPublished?: Int | null
  yearPublished_not?: Int | null
  yearPublished_in?: Int[] | Int | null
  yearPublished_not_in?: Int[] | Int | null
  yearPublished_lt?: Int | null
  yearPublished_lte?: Int | null
  yearPublished_gt?: Int | null
  yearPublished_gte?: Int | null
  lastReload?: DateTime | null
  lastReload_not?: DateTime | null
  lastReload_in?: DateTime[] | DateTime | null
  lastReload_not_in?: DateTime[] | DateTime | null
  lastReload_lt?: DateTime | null
  lastReload_lte?: DateTime | null
  lastReload_gt?: DateTime | null
  lastReload_gte?: DateTime | null
  gameresults_every?: GameresultWhereInput | null
  gameresults_some?: GameresultWhereInput | null
  gameresults_none?: GameresultWhereInput | null
}

export interface BggInfoWhereUniqueInput {
  bggid?: Int | null
  id?: ID_Input | null
}

export interface FileCreateInput {
  contentType: String
  id?: ID_Input | null
  name: String
  secret: String
  size: Int
  url: String
}

export interface FileSubscriptionWhereInput {
  AND?: FileSubscriptionWhereInput[] | FileSubscriptionWhereInput | null
  OR?: FileSubscriptionWhereInput[] | FileSubscriptionWhereInput | null
  NOT?: FileSubscriptionWhereInput[] | FileSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: FileWhereInput | null
}

export interface FileUpdateInput {
  contentType?: String | null
  name?: String | null
  secret?: String | null
  size?: Int | null
  url?: String | null
}

export interface FileUpdateManyMutationInput {
  contentType?: String | null
  name?: String | null
  secret?: String | null
  size?: Int | null
  url?: String | null
}

export interface FileWhereInput {
  AND?: FileWhereInput[] | FileWhereInput | null
  OR?: FileWhereInput[] | FileWhereInput | null
  NOT?: FileWhereInput[] | FileWhereInput | null
  contentType?: String | null
  contentType_not?: String | null
  contentType_in?: String[] | String | null
  contentType_not_in?: String[] | String | null
  contentType_lt?: String | null
  contentType_lte?: String | null
  contentType_gt?: String | null
  contentType_gte?: String | null
  contentType_contains?: String | null
  contentType_not_contains?: String | null
  contentType_starts_with?: String | null
  contentType_not_starts_with?: String | null
  contentType_ends_with?: String | null
  contentType_not_ends_with?: String | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  secret?: String | null
  secret_not?: String | null
  secret_in?: String[] | String | null
  secret_not_in?: String[] | String | null
  secret_lt?: String | null
  secret_lte?: String | null
  secret_gt?: String | null
  secret_gte?: String | null
  secret_contains?: String | null
  secret_not_contains?: String | null
  secret_starts_with?: String | null
  secret_not_starts_with?: String | null
  secret_ends_with?: String | null
  secret_not_ends_with?: String | null
  size?: Int | null
  size_not?: Int | null
  size_in?: Int[] | Int | null
  size_not_in?: Int[] | Int | null
  size_lt?: Int | null
  size_lte?: Int | null
  size_gt?: Int | null
  size_gte?: Int | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  url?: String | null
  url_not?: String | null
  url_in?: String[] | String | null
  url_not_in?: String[] | String | null
  url_lt?: String | null
  url_lte?: String | null
  url_gt?: String | null
  url_gte?: String | null
  url_contains?: String | null
  url_not_contains?: String | null
  url_starts_with?: String | null
  url_not_starts_with?: String | null
  url_ends_with?: String | null
  url_not_ends_with?: String | null
}

export interface FileWhereUniqueInput {
  id?: ID_Input | null
  secret?: String | null
  url?: String | null
}

export interface GameresultCreateInput {
  id?: ID_Input | null
  numPlayers?: Int | null
  time: DateTime
  bggInfo?: BggInfoCreateOneWithoutGameresultsInput | null
  scores?: ScoreCreateManyWithoutGameresultInput | null
  tournament: TournamentCreateOneWithoutGameresultsInput
}

export interface GameresultCreateManyWithoutBggInfoInput {
  create?:
    | GameresultCreateWithoutBggInfoInput[]
    | GameresultCreateWithoutBggInfoInput
    | null
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
}

export interface GameresultCreateManyWithoutTournamentInput {
  create?:
    | GameresultCreateWithoutTournamentInput[]
    | GameresultCreateWithoutTournamentInput
    | null
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
}

export interface GameresultCreateOneWithoutScoresInput {
  create?: GameresultCreateWithoutScoresInput | null
  connect?: GameresultWhereUniqueInput | null
}

export interface GameresultCreateWithoutBggInfoInput {
  id?: ID_Input | null
  numPlayers?: Int | null
  time: DateTime
  scores?: ScoreCreateManyWithoutGameresultInput | null
  tournament: TournamentCreateOneWithoutGameresultsInput
}

export interface GameresultCreateWithoutScoresInput {
  id?: ID_Input | null
  numPlayers?: Int | null
  time: DateTime
  bggInfo?: BggInfoCreateOneWithoutGameresultsInput | null
  tournament: TournamentCreateOneWithoutGameresultsInput
}

export interface GameresultCreateWithoutTournamentInput {
  id?: ID_Input | null
  numPlayers?: Int | null
  time: DateTime
  bggInfo?: BggInfoCreateOneWithoutGameresultsInput | null
  scores?: ScoreCreateManyWithoutGameresultInput | null
}

export interface GameresultScalarWhereInput {
  AND?: GameresultScalarWhereInput[] | GameresultScalarWhereInput | null
  OR?: GameresultScalarWhereInput[] | GameresultScalarWhereInput | null
  NOT?: GameresultScalarWhereInput[] | GameresultScalarWhereInput | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  numPlayers?: Int | null
  numPlayers_not?: Int | null
  numPlayers_in?: Int[] | Int | null
  numPlayers_not_in?: Int[] | Int | null
  numPlayers_lt?: Int | null
  numPlayers_lte?: Int | null
  numPlayers_gt?: Int | null
  numPlayers_gte?: Int | null
  time?: DateTime | null
  time_not?: DateTime | null
  time_in?: DateTime[] | DateTime | null
  time_not_in?: DateTime[] | DateTime | null
  time_lt?: DateTime | null
  time_lte?: DateTime | null
  time_gt?: DateTime | null
  time_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
}

export interface GameresultSubscriptionWhereInput {
  AND?:
    | GameresultSubscriptionWhereInput[]
    | GameresultSubscriptionWhereInput
    | null
  OR?:
    | GameresultSubscriptionWhereInput[]
    | GameresultSubscriptionWhereInput
    | null
  NOT?:
    | GameresultSubscriptionWhereInput[]
    | GameresultSubscriptionWhereInput
    | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: GameresultWhereInput | null
}

export interface GameresultUpdateInput {
  numPlayers?: Int | null
  time?: DateTime | null
  bggInfo?: BggInfoUpdateOneWithoutGameresultsInput | null
  scores?: ScoreUpdateManyWithoutGameresultInput | null
  tournament?: TournamentUpdateOneRequiredWithoutGameresultsInput | null
}

export interface GameresultUpdateManyDataInput {
  numPlayers?: Int | null
  time?: DateTime | null
}

export interface GameresultUpdateManyMutationInput {
  numPlayers?: Int | null
  time?: DateTime | null
}

export interface GameresultUpdateManyWithoutBggInfoInput {
  create?:
    | GameresultCreateWithoutBggInfoInput[]
    | GameresultCreateWithoutBggInfoInput
    | null
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  set?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  disconnect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  delete?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  update?:
    | GameresultUpdateWithWhereUniqueWithoutBggInfoInput[]
    | GameresultUpdateWithWhereUniqueWithoutBggInfoInput
    | null
  updateMany?:
    | GameresultUpdateManyWithWhereNestedInput[]
    | GameresultUpdateManyWithWhereNestedInput
    | null
  deleteMany?: GameresultScalarWhereInput[] | GameresultScalarWhereInput | null
  upsert?:
    | GameresultUpsertWithWhereUniqueWithoutBggInfoInput[]
    | GameresultUpsertWithWhereUniqueWithoutBggInfoInput
    | null
}

export interface GameresultUpdateManyWithoutTournamentInput {
  create?:
    | GameresultCreateWithoutTournamentInput[]
    | GameresultCreateWithoutTournamentInput
    | null
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  set?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  disconnect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  delete?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput | null
  update?:
    | GameresultUpdateWithWhereUniqueWithoutTournamentInput[]
    | GameresultUpdateWithWhereUniqueWithoutTournamentInput
    | null
  updateMany?:
    | GameresultUpdateManyWithWhereNestedInput[]
    | GameresultUpdateManyWithWhereNestedInput
    | null
  deleteMany?: GameresultScalarWhereInput[] | GameresultScalarWhereInput | null
  upsert?:
    | GameresultUpsertWithWhereUniqueWithoutTournamentInput[]
    | GameresultUpsertWithWhereUniqueWithoutTournamentInput
    | null
}

export interface GameresultUpdateManyWithWhereNestedInput {
  where: GameresultScalarWhereInput
  data: GameresultUpdateManyDataInput
}

export interface GameresultUpdateOneWithoutScoresInput {
  create?: GameresultCreateWithoutScoresInput | null
  connect?: GameresultWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: GameresultUpdateWithoutScoresDataInput | null
  upsert?: GameresultUpsertWithoutScoresInput | null
}

export interface GameresultUpdateWithoutBggInfoDataInput {
  numPlayers?: Int | null
  time?: DateTime | null
  scores?: ScoreUpdateManyWithoutGameresultInput | null
  tournament?: TournamentUpdateOneRequiredWithoutGameresultsInput | null
}

export interface GameresultUpdateWithoutScoresDataInput {
  numPlayers?: Int | null
  time?: DateTime | null
  bggInfo?: BggInfoUpdateOneWithoutGameresultsInput | null
  tournament?: TournamentUpdateOneRequiredWithoutGameresultsInput | null
}

export interface GameresultUpdateWithoutTournamentDataInput {
  numPlayers?: Int | null
  time?: DateTime | null
  bggInfo?: BggInfoUpdateOneWithoutGameresultsInput | null
  scores?: ScoreUpdateManyWithoutGameresultInput | null
}

export interface GameresultUpdateWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput
  data: GameresultUpdateWithoutBggInfoDataInput
}

export interface GameresultUpdateWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput
  data: GameresultUpdateWithoutTournamentDataInput
}

export interface GameresultUpsertWithoutScoresInput {
  update: GameresultUpdateWithoutScoresDataInput
  create: GameresultCreateWithoutScoresInput
}

export interface GameresultUpsertWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput
  update: GameresultUpdateWithoutBggInfoDataInput
  create: GameresultCreateWithoutBggInfoInput
}

export interface GameresultUpsertWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput
  update: GameresultUpdateWithoutTournamentDataInput
  create: GameresultCreateWithoutTournamentInput
}

export interface GameresultWhereInput {
  AND?: GameresultWhereInput[] | GameresultWhereInput | null
  OR?: GameresultWhereInput[] | GameresultWhereInput | null
  NOT?: GameresultWhereInput[] | GameresultWhereInput | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  numPlayers?: Int | null
  numPlayers_not?: Int | null
  numPlayers_in?: Int[] | Int | null
  numPlayers_not_in?: Int[] | Int | null
  numPlayers_lt?: Int | null
  numPlayers_lte?: Int | null
  numPlayers_gt?: Int | null
  numPlayers_gte?: Int | null
  time?: DateTime | null
  time_not?: DateTime | null
  time_in?: DateTime[] | DateTime | null
  time_not_in?: DateTime[] | DateTime | null
  time_lt?: DateTime | null
  time_lte?: DateTime | null
  time_gt?: DateTime | null
  time_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  bggInfo?: BggInfoWhereInput | null
  scores_every?: ScoreWhereInput | null
  scores_some?: ScoreWhereInput | null
  scores_none?: ScoreWhereInput | null
  tournament?: TournamentWhereInput | null
}

export interface GameresultWhereUniqueInput {
  id?: ID_Input | null
}

export interface InvitationCreateInput {
  id?: ID_Input | null
  token: String
  state?: InvitationState | null
  email: String
  inviter: UserCreateOneInput
  tournament: TournamentCreateOneWithoutInvitationsInput
}

export interface InvitationCreateManyWithoutTournamentInput {
  create?:
    | InvitationCreateWithoutTournamentInput[]
    | InvitationCreateWithoutTournamentInput
    | null
  connect?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput | null
}

export interface InvitationCreateWithoutTournamentInput {
  id?: ID_Input | null
  token: String
  state?: InvitationState | null
  email: String
  inviter: UserCreateOneInput
}

export interface InvitationScalarWhereInput {
  AND?: InvitationScalarWhereInput[] | InvitationScalarWhereInput | null
  OR?: InvitationScalarWhereInput[] | InvitationScalarWhereInput | null
  NOT?: InvitationScalarWhereInput[] | InvitationScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  token?: String | null
  token_not?: String | null
  token_in?: String[] | String | null
  token_not_in?: String[] | String | null
  token_lt?: String | null
  token_lte?: String | null
  token_gt?: String | null
  token_gte?: String | null
  token_contains?: String | null
  token_not_contains?: String | null
  token_starts_with?: String | null
  token_not_starts_with?: String | null
  token_ends_with?: String | null
  token_not_ends_with?: String | null
  state?: InvitationState | null
  state_not?: InvitationState | null
  state_in?: InvitationState[] | InvitationState | null
  state_not_in?: InvitationState[] | InvitationState | null
  email?: String | null
  email_not?: String | null
  email_in?: String[] | String | null
  email_not_in?: String[] | String | null
  email_lt?: String | null
  email_lte?: String | null
  email_gt?: String | null
  email_gte?: String | null
  email_contains?: String | null
  email_not_contains?: String | null
  email_starts_with?: String | null
  email_not_starts_with?: String | null
  email_ends_with?: String | null
  email_not_ends_with?: String | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
}

export interface InvitationSubscriptionWhereInput {
  AND?:
    | InvitationSubscriptionWhereInput[]
    | InvitationSubscriptionWhereInput
    | null
  OR?:
    | InvitationSubscriptionWhereInput[]
    | InvitationSubscriptionWhereInput
    | null
  NOT?:
    | InvitationSubscriptionWhereInput[]
    | InvitationSubscriptionWhereInput
    | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: InvitationWhereInput | null
}

export interface InvitationUpdateInput {
  token?: String | null
  state?: InvitationState | null
  email?: String | null
  inviter?: UserUpdateOneRequiredInput | null
  tournament?: TournamentUpdateOneRequiredWithoutInvitationsInput | null
}

export interface InvitationUpdateManyDataInput {
  token?: String | null
  state?: InvitationState | null
  email?: String | null
}

export interface InvitationUpdateManyMutationInput {
  token?: String | null
  state?: InvitationState | null
  email?: String | null
}

export interface InvitationUpdateManyWithoutTournamentInput {
  create?:
    | InvitationCreateWithoutTournamentInput[]
    | InvitationCreateWithoutTournamentInput
    | null
  connect?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput | null
  set?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput | null
  disconnect?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput | null
  delete?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput | null
  update?:
    | InvitationUpdateWithWhereUniqueWithoutTournamentInput[]
    | InvitationUpdateWithWhereUniqueWithoutTournamentInput
    | null
  updateMany?:
    | InvitationUpdateManyWithWhereNestedInput[]
    | InvitationUpdateManyWithWhereNestedInput
    | null
  deleteMany?: InvitationScalarWhereInput[] | InvitationScalarWhereInput | null
  upsert?:
    | InvitationUpsertWithWhereUniqueWithoutTournamentInput[]
    | InvitationUpsertWithWhereUniqueWithoutTournamentInput
    | null
}

export interface InvitationUpdateManyWithWhereNestedInput {
  where: InvitationScalarWhereInput
  data: InvitationUpdateManyDataInput
}

export interface InvitationUpdateWithoutTournamentDataInput {
  token?: String | null
  state?: InvitationState | null
  email?: String | null
  inviter?: UserUpdateOneRequiredInput | null
}

export interface InvitationUpdateWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput
  data: InvitationUpdateWithoutTournamentDataInput
}

export interface InvitationUpsertWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput
  update: InvitationUpdateWithoutTournamentDataInput
  create: InvitationCreateWithoutTournamentInput
}

export interface InvitationWhereInput {
  AND?: InvitationWhereInput[] | InvitationWhereInput | null
  OR?: InvitationWhereInput[] | InvitationWhereInput | null
  NOT?: InvitationWhereInput[] | InvitationWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  token?: String | null
  token_not?: String | null
  token_in?: String[] | String | null
  token_not_in?: String[] | String | null
  token_lt?: String | null
  token_lte?: String | null
  token_gt?: String | null
  token_gte?: String | null
  token_contains?: String | null
  token_not_contains?: String | null
  token_starts_with?: String | null
  token_not_starts_with?: String | null
  token_ends_with?: String | null
  token_not_ends_with?: String | null
  state?: InvitationState | null
  state_not?: InvitationState | null
  state_in?: InvitationState[] | InvitationState | null
  state_not_in?: InvitationState[] | InvitationState | null
  email?: String | null
  email_not?: String | null
  email_in?: String[] | String | null
  email_not_in?: String[] | String | null
  email_lt?: String | null
  email_lte?: String | null
  email_gt?: String | null
  email_gte?: String | null
  email_contains?: String | null
  email_not_contains?: String | null
  email_starts_with?: String | null
  email_not_starts_with?: String | null
  email_ends_with?: String | null
  email_not_ends_with?: String | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  inviter?: UserWhereInput | null
  tournament?: TournamentWhereInput | null
}

export interface InvitationWhereUniqueInput {
  id?: ID_Input | null
  token?: String | null
}

export interface PlayerCreateInput {
  id?: ID_Input | null
  name: String
  scores?: ScoreCreateManyWithoutPlayerInput | null
  tournament: TournamentCreateOneWithoutPlayersInput
}

export interface PlayerCreateManyWithoutTournamentInput {
  create?:
    | PlayerCreateWithoutTournamentInput[]
    | PlayerCreateWithoutTournamentInput
    | null
  connect?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput | null
}

export interface PlayerCreateOneWithoutScoresInput {
  create?: PlayerCreateWithoutScoresInput | null
  connect?: PlayerWhereUniqueInput | null
}

export interface PlayerCreateWithoutScoresInput {
  id?: ID_Input | null
  name: String
  tournament: TournamentCreateOneWithoutPlayersInput
}

export interface PlayerCreateWithoutTournamentInput {
  id?: ID_Input | null
  name: String
  scores?: ScoreCreateManyWithoutPlayerInput | null
}

export interface PlayerScalarWhereInput {
  AND?: PlayerScalarWhereInput[] | PlayerScalarWhereInput | null
  OR?: PlayerScalarWhereInput[] | PlayerScalarWhereInput | null
  NOT?: PlayerScalarWhereInput[] | PlayerScalarWhereInput | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
}

export interface PlayerSubscriptionWhereInput {
  AND?: PlayerSubscriptionWhereInput[] | PlayerSubscriptionWhereInput | null
  OR?: PlayerSubscriptionWhereInput[] | PlayerSubscriptionWhereInput | null
  NOT?: PlayerSubscriptionWhereInput[] | PlayerSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PlayerWhereInput | null
}

export interface PlayerUpdateInput {
  name?: String | null
  scores?: ScoreUpdateManyWithoutPlayerInput | null
  tournament?: TournamentUpdateOneRequiredWithoutPlayersInput | null
}

export interface PlayerUpdateManyDataInput {
  name?: String | null
}

export interface PlayerUpdateManyMutationInput {
  name?: String | null
}

export interface PlayerUpdateManyWithoutTournamentInput {
  create?:
    | PlayerCreateWithoutTournamentInput[]
    | PlayerCreateWithoutTournamentInput
    | null
  connect?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput | null
  set?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput | null
  disconnect?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput | null
  delete?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput | null
  update?:
    | PlayerUpdateWithWhereUniqueWithoutTournamentInput[]
    | PlayerUpdateWithWhereUniqueWithoutTournamentInput
    | null
  updateMany?:
    | PlayerUpdateManyWithWhereNestedInput[]
    | PlayerUpdateManyWithWhereNestedInput
    | null
  deleteMany?: PlayerScalarWhereInput[] | PlayerScalarWhereInput | null
  upsert?:
    | PlayerUpsertWithWhereUniqueWithoutTournamentInput[]
    | PlayerUpsertWithWhereUniqueWithoutTournamentInput
    | null
}

export interface PlayerUpdateManyWithWhereNestedInput {
  where: PlayerScalarWhereInput
  data: PlayerUpdateManyDataInput
}

export interface PlayerUpdateOneRequiredWithoutScoresInput {
  create?: PlayerCreateWithoutScoresInput | null
  connect?: PlayerWhereUniqueInput | null
  update?: PlayerUpdateWithoutScoresDataInput | null
  upsert?: PlayerUpsertWithoutScoresInput | null
}

export interface PlayerUpdateWithoutScoresDataInput {
  name?: String | null
  tournament?: TournamentUpdateOneRequiredWithoutPlayersInput | null
}

export interface PlayerUpdateWithoutTournamentDataInput {
  name?: String | null
  scores?: ScoreUpdateManyWithoutPlayerInput | null
}

export interface PlayerUpdateWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput
  data: PlayerUpdateWithoutTournamentDataInput
}

export interface PlayerUpsertWithoutScoresInput {
  update: PlayerUpdateWithoutScoresDataInput
  create: PlayerCreateWithoutScoresInput
}

export interface PlayerUpsertWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput
  update: PlayerUpdateWithoutTournamentDataInput
  create: PlayerCreateWithoutTournamentInput
}

export interface PlayerWhereInput {
  AND?: PlayerWhereInput[] | PlayerWhereInput | null
  OR?: PlayerWhereInput[] | PlayerWhereInput | null
  NOT?: PlayerWhereInput[] | PlayerWhereInput | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  scores_every?: ScoreWhereInput | null
  scores_some?: ScoreWhereInput | null
  scores_none?: ScoreWhereInput | null
  tournament?: TournamentWhereInput | null
}

export interface PlayerWhereUniqueInput {
  id?: ID_Input | null
}

export interface ScoreCreateInput {
  id?: ID_Input | null
  score: Int
  gameresult?: GameresultCreateOneWithoutScoresInput | null
  player: PlayerCreateOneWithoutScoresInput
}

export interface ScoreCreateManyWithoutGameresultInput {
  create?:
    | ScoreCreateWithoutGameresultInput[]
    | ScoreCreateWithoutGameresultInput
    | null
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
}

export interface ScoreCreateManyWithoutPlayerInput {
  create?:
    | ScoreCreateWithoutPlayerInput[]
    | ScoreCreateWithoutPlayerInput
    | null
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
}

export interface ScoreCreateWithoutGameresultInput {
  id?: ID_Input | null
  score: Int
  player: PlayerCreateOneWithoutScoresInput
}

export interface ScoreCreateWithoutPlayerInput {
  id?: ID_Input | null
  score: Int
  gameresult?: GameresultCreateOneWithoutScoresInput | null
}

export interface ScoreScalarWhereInput {
  AND?: ScoreScalarWhereInput[] | ScoreScalarWhereInput | null
  OR?: ScoreScalarWhereInput[] | ScoreScalarWhereInput | null
  NOT?: ScoreScalarWhereInput[] | ScoreScalarWhereInput | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  score?: Int | null
  score_not?: Int | null
  score_in?: Int[] | Int | null
  score_not_in?: Int[] | Int | null
  score_lt?: Int | null
  score_lte?: Int | null
  score_gt?: Int | null
  score_gte?: Int | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
}

export interface ScoreSubscriptionWhereInput {
  AND?: ScoreSubscriptionWhereInput[] | ScoreSubscriptionWhereInput | null
  OR?: ScoreSubscriptionWhereInput[] | ScoreSubscriptionWhereInput | null
  NOT?: ScoreSubscriptionWhereInput[] | ScoreSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: ScoreWhereInput | null
}

export interface ScoreUpdateInput {
  score?: Int | null
  gameresult?: GameresultUpdateOneWithoutScoresInput | null
  player?: PlayerUpdateOneRequiredWithoutScoresInput | null
}

export interface ScoreUpdateManyDataInput {
  score?: Int | null
}

export interface ScoreUpdateManyMutationInput {
  score?: Int | null
}

export interface ScoreUpdateManyWithoutGameresultInput {
  create?:
    | ScoreCreateWithoutGameresultInput[]
    | ScoreCreateWithoutGameresultInput
    | null
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  set?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  disconnect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  delete?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  update?:
    | ScoreUpdateWithWhereUniqueWithoutGameresultInput[]
    | ScoreUpdateWithWhereUniqueWithoutGameresultInput
    | null
  updateMany?:
    | ScoreUpdateManyWithWhereNestedInput[]
    | ScoreUpdateManyWithWhereNestedInput
    | null
  deleteMany?: ScoreScalarWhereInput[] | ScoreScalarWhereInput | null
  upsert?:
    | ScoreUpsertWithWhereUniqueWithoutGameresultInput[]
    | ScoreUpsertWithWhereUniqueWithoutGameresultInput
    | null
}

export interface ScoreUpdateManyWithoutPlayerInput {
  create?:
    | ScoreCreateWithoutPlayerInput[]
    | ScoreCreateWithoutPlayerInput
    | null
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  set?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  disconnect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  delete?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput | null
  update?:
    | ScoreUpdateWithWhereUniqueWithoutPlayerInput[]
    | ScoreUpdateWithWhereUniqueWithoutPlayerInput
    | null
  updateMany?:
    | ScoreUpdateManyWithWhereNestedInput[]
    | ScoreUpdateManyWithWhereNestedInput
    | null
  deleteMany?: ScoreScalarWhereInput[] | ScoreScalarWhereInput | null
  upsert?:
    | ScoreUpsertWithWhereUniqueWithoutPlayerInput[]
    | ScoreUpsertWithWhereUniqueWithoutPlayerInput
    | null
}

export interface ScoreUpdateManyWithWhereNestedInput {
  where: ScoreScalarWhereInput
  data: ScoreUpdateManyDataInput
}

export interface ScoreUpdateWithoutGameresultDataInput {
  score?: Int | null
  player?: PlayerUpdateOneRequiredWithoutScoresInput | null
}

export interface ScoreUpdateWithoutPlayerDataInput {
  score?: Int | null
  gameresult?: GameresultUpdateOneWithoutScoresInput | null
}

export interface ScoreUpdateWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput
  data: ScoreUpdateWithoutGameresultDataInput
}

export interface ScoreUpdateWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput
  data: ScoreUpdateWithoutPlayerDataInput
}

export interface ScoreUpsertWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput
  update: ScoreUpdateWithoutGameresultDataInput
  create: ScoreCreateWithoutGameresultInput
}

export interface ScoreUpsertWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput
  update: ScoreUpdateWithoutPlayerDataInput
  create: ScoreCreateWithoutPlayerInput
}

export interface ScoreWhereInput {
  AND?: ScoreWhereInput[] | ScoreWhereInput | null
  OR?: ScoreWhereInput[] | ScoreWhereInput | null
  NOT?: ScoreWhereInput[] | ScoreWhereInput | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  score?: Int | null
  score_not?: Int | null
  score_in?: Int[] | Int | null
  score_not_in?: Int[] | Int | null
  score_lt?: Int | null
  score_lte?: Int | null
  score_gt?: Int | null
  score_gte?: Int | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  gameresult?: GameresultWhereInput | null
  player?: PlayerWhereInput | null
}

export interface ScoreWhereUniqueInput {
  id?: ID_Input | null
}

export interface TournamentCreateInput {
  id?: ID_Input | null
  slug: String
  name: String
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultCreateManyWithoutTournamentInput | null
  players?: PlayerCreateManyWithoutTournamentInput | null
  members?: TournamentMemberCreateManyWithoutTournamentInput | null
  invitations?: InvitationCreateManyWithoutTournamentInput | null
}

export interface TournamentCreateOneWithoutGameresultsInput {
  create?: TournamentCreateWithoutGameresultsInput | null
  connect?: TournamentWhereUniqueInput | null
}

export interface TournamentCreateOneWithoutInvitationsInput {
  create?: TournamentCreateWithoutInvitationsInput | null
  connect?: TournamentWhereUniqueInput | null
}

export interface TournamentCreateOneWithoutMembersInput {
  create?: TournamentCreateWithoutMembersInput | null
  connect?: TournamentWhereUniqueInput | null
}

export interface TournamentCreateOneWithoutPlayersInput {
  create?: TournamentCreateWithoutPlayersInput | null
  connect?: TournamentWhereUniqueInput | null
}

export interface TournamentCreateWithoutGameresultsInput {
  id?: ID_Input | null
  slug: String
  name: String
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  players?: PlayerCreateManyWithoutTournamentInput | null
  members?: TournamentMemberCreateManyWithoutTournamentInput | null
  invitations?: InvitationCreateManyWithoutTournamentInput | null
}

export interface TournamentCreateWithoutInvitationsInput {
  id?: ID_Input | null
  slug: String
  name: String
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultCreateManyWithoutTournamentInput | null
  players?: PlayerCreateManyWithoutTournamentInput | null
  members?: TournamentMemberCreateManyWithoutTournamentInput | null
}

export interface TournamentCreateWithoutMembersInput {
  id?: ID_Input | null
  slug: String
  name: String
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultCreateManyWithoutTournamentInput | null
  players?: PlayerCreateManyWithoutTournamentInput | null
  invitations?: InvitationCreateManyWithoutTournamentInput | null
}

export interface TournamentCreateWithoutPlayersInput {
  id?: ID_Input | null
  slug: String
  name: String
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultCreateManyWithoutTournamentInput | null
  members?: TournamentMemberCreateManyWithoutTournamentInput | null
  invitations?: InvitationCreateManyWithoutTournamentInput | null
}

export interface TournamentMemberCreateInput {
  id?: ID_Input | null
  role?: TournamentMemberRole | null
  tournament: TournamentCreateOneWithoutMembersInput
  user: UserCreateOneWithoutTournamentsInput
}

export interface TournamentMemberCreateManyWithoutTournamentInput {
  create?:
    | TournamentMemberCreateWithoutTournamentInput[]
    | TournamentMemberCreateWithoutTournamentInput
    | null
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
}

export interface TournamentMemberCreateManyWithoutUserInput {
  create?:
    | TournamentMemberCreateWithoutUserInput[]
    | TournamentMemberCreateWithoutUserInput
    | null
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
}

export interface TournamentMemberCreateWithoutTournamentInput {
  id?: ID_Input | null
  role?: TournamentMemberRole | null
  user: UserCreateOneWithoutTournamentsInput
}

export interface TournamentMemberCreateWithoutUserInput {
  id?: ID_Input | null
  role?: TournamentMemberRole | null
  tournament: TournamentCreateOneWithoutMembersInput
}

export interface TournamentMemberScalarWhereInput {
  AND?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
    | null
  OR?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
    | null
  NOT?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
    | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  role?: TournamentMemberRole | null
  role_not?: TournamentMemberRole | null
  role_in?: TournamentMemberRole[] | TournamentMemberRole | null
  role_not_in?: TournamentMemberRole[] | TournamentMemberRole | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
}

export interface TournamentMemberSubscriptionWhereInput {
  AND?:
    | TournamentMemberSubscriptionWhereInput[]
    | TournamentMemberSubscriptionWhereInput
    | null
  OR?:
    | TournamentMemberSubscriptionWhereInput[]
    | TournamentMemberSubscriptionWhereInput
    | null
  NOT?:
    | TournamentMemberSubscriptionWhereInput[]
    | TournamentMemberSubscriptionWhereInput
    | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: TournamentMemberWhereInput | null
}

export interface TournamentMemberUpdateInput {
  role?: TournamentMemberRole | null
  tournament?: TournamentUpdateOneRequiredWithoutMembersInput | null
  user?: UserUpdateOneRequiredWithoutTournamentsInput | null
}

export interface TournamentMemberUpdateManyDataInput {
  role?: TournamentMemberRole | null
}

export interface TournamentMemberUpdateManyMutationInput {
  role?: TournamentMemberRole | null
}

export interface TournamentMemberUpdateManyWithoutTournamentInput {
  create?:
    | TournamentMemberCreateWithoutTournamentInput[]
    | TournamentMemberCreateWithoutTournamentInput
    | null
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  set?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  disconnect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  delete?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  update?:
    | TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput[]
    | TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput
    | null
  updateMany?:
    | TournamentMemberUpdateManyWithWhereNestedInput[]
    | TournamentMemberUpdateManyWithWhereNestedInput
    | null
  deleteMany?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
    | null
  upsert?:
    | TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput[]
    | TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput
    | null
}

export interface TournamentMemberUpdateManyWithoutUserInput {
  create?:
    | TournamentMemberCreateWithoutUserInput[]
    | TournamentMemberCreateWithoutUserInput
    | null
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  set?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  disconnect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  delete?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
    | null
  update?:
    | TournamentMemberUpdateWithWhereUniqueWithoutUserInput[]
    | TournamentMemberUpdateWithWhereUniqueWithoutUserInput
    | null
  updateMany?:
    | TournamentMemberUpdateManyWithWhereNestedInput[]
    | TournamentMemberUpdateManyWithWhereNestedInput
    | null
  deleteMany?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
    | null
  upsert?:
    | TournamentMemberUpsertWithWhereUniqueWithoutUserInput[]
    | TournamentMemberUpsertWithWhereUniqueWithoutUserInput
    | null
}

export interface TournamentMemberUpdateManyWithWhereNestedInput {
  where: TournamentMemberScalarWhereInput
  data: TournamentMemberUpdateManyDataInput
}

export interface TournamentMemberUpdateWithoutTournamentDataInput {
  role?: TournamentMemberRole | null
  user?: UserUpdateOneRequiredWithoutTournamentsInput | null
}

export interface TournamentMemberUpdateWithoutUserDataInput {
  role?: TournamentMemberRole | null
  tournament?: TournamentUpdateOneRequiredWithoutMembersInput | null
}

export interface TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput
  data: TournamentMemberUpdateWithoutTournamentDataInput
}

export interface TournamentMemberUpdateWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput
  data: TournamentMemberUpdateWithoutUserDataInput
}

export interface TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput
  update: TournamentMemberUpdateWithoutTournamentDataInput
  create: TournamentMemberCreateWithoutTournamentInput
}

export interface TournamentMemberUpsertWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput
  update: TournamentMemberUpdateWithoutUserDataInput
  create: TournamentMemberCreateWithoutUserInput
}

export interface TournamentMemberWhereInput {
  AND?: TournamentMemberWhereInput[] | TournamentMemberWhereInput | null
  OR?: TournamentMemberWhereInput[] | TournamentMemberWhereInput | null
  NOT?: TournamentMemberWhereInput[] | TournamentMemberWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  role?: TournamentMemberRole | null
  role_not?: TournamentMemberRole | null
  role_in?: TournamentMemberRole[] | TournamentMemberRole | null
  role_not_in?: TournamentMemberRole[] | TournamentMemberRole | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  tournament?: TournamentWhereInput | null
  user?: UserWhereInput | null
}

export interface TournamentMemberWhereUniqueInput {
  id?: ID_Input | null
}

export interface TournamentSubscriptionWhereInput {
  AND?:
    | TournamentSubscriptionWhereInput[]
    | TournamentSubscriptionWhereInput
    | null
  OR?:
    | TournamentSubscriptionWhereInput[]
    | TournamentSubscriptionWhereInput
    | null
  NOT?:
    | TournamentSubscriptionWhereInput[]
    | TournamentSubscriptionWhereInput
    | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: TournamentWhereInput | null
}

export interface TournamentUpdateInput {
  slug?: String | null
  name?: String | null
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultUpdateManyWithoutTournamentInput | null
  players?: PlayerUpdateManyWithoutTournamentInput | null
  members?: TournamentMemberUpdateManyWithoutTournamentInput | null
  invitations?: InvitationUpdateManyWithoutTournamentInput | null
}

export interface TournamentUpdateManyMutationInput {
  slug?: String | null
  name?: String | null
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
}

export interface TournamentUpdateOneRequiredWithoutGameresultsInput {
  create?: TournamentCreateWithoutGameresultsInput | null
  connect?: TournamentWhereUniqueInput | null
  update?: TournamentUpdateWithoutGameresultsDataInput | null
  upsert?: TournamentUpsertWithoutGameresultsInput | null
}

export interface TournamentUpdateOneRequiredWithoutInvitationsInput {
  create?: TournamentCreateWithoutInvitationsInput | null
  connect?: TournamentWhereUniqueInput | null
  update?: TournamentUpdateWithoutInvitationsDataInput | null
  upsert?: TournamentUpsertWithoutInvitationsInput | null
}

export interface TournamentUpdateOneRequiredWithoutMembersInput {
  create?: TournamentCreateWithoutMembersInput | null
  connect?: TournamentWhereUniqueInput | null
  update?: TournamentUpdateWithoutMembersDataInput | null
  upsert?: TournamentUpsertWithoutMembersInput | null
}

export interface TournamentUpdateOneRequiredWithoutPlayersInput {
  create?: TournamentCreateWithoutPlayersInput | null
  connect?: TournamentWhereUniqueInput | null
  update?: TournamentUpdateWithoutPlayersDataInput | null
  upsert?: TournamentUpsertWithoutPlayersInput | null
}

export interface TournamentUpdateWithoutGameresultsDataInput {
  slug?: String | null
  name?: String | null
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  players?: PlayerUpdateManyWithoutTournamentInput | null
  members?: TournamentMemberUpdateManyWithoutTournamentInput | null
  invitations?: InvitationUpdateManyWithoutTournamentInput | null
}

export interface TournamentUpdateWithoutInvitationsDataInput {
  slug?: String | null
  name?: String | null
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultUpdateManyWithoutTournamentInput | null
  players?: PlayerUpdateManyWithoutTournamentInput | null
  members?: TournamentMemberUpdateManyWithoutTournamentInput | null
}

export interface TournamentUpdateWithoutMembersDataInput {
  slug?: String | null
  name?: String | null
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultUpdateManyWithoutTournamentInput | null
  players?: PlayerUpdateManyWithoutTournamentInput | null
  invitations?: InvitationUpdateManyWithoutTournamentInput | null
}

export interface TournamentUpdateWithoutPlayersDataInput {
  slug?: String | null
  name?: String | null
  lastGameresultAt?: DateTime | null
  numResults?: Int | null
  numPlayers?: Int | null
  numGames?: Int | null
  gameresults?: GameresultUpdateManyWithoutTournamentInput | null
  members?: TournamentMemberUpdateManyWithoutTournamentInput | null
  invitations?: InvitationUpdateManyWithoutTournamentInput | null
}

export interface TournamentUpsertWithoutGameresultsInput {
  update: TournamentUpdateWithoutGameresultsDataInput
  create: TournamentCreateWithoutGameresultsInput
}

export interface TournamentUpsertWithoutInvitationsInput {
  update: TournamentUpdateWithoutInvitationsDataInput
  create: TournamentCreateWithoutInvitationsInput
}

export interface TournamentUpsertWithoutMembersInput {
  update: TournamentUpdateWithoutMembersDataInput
  create: TournamentCreateWithoutMembersInput
}

export interface TournamentUpsertWithoutPlayersInput {
  update: TournamentUpdateWithoutPlayersDataInput
  create: TournamentCreateWithoutPlayersInput
}

export interface TournamentWhereInput {
  AND?: TournamentWhereInput[] | TournamentWhereInput | null
  OR?: TournamentWhereInput[] | TournamentWhereInput | null
  NOT?: TournamentWhereInput[] | TournamentWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  slug?: String | null
  slug_not?: String | null
  slug_in?: String[] | String | null
  slug_not_in?: String[] | String | null
  slug_lt?: String | null
  slug_lte?: String | null
  slug_gt?: String | null
  slug_gte?: String | null
  slug_contains?: String | null
  slug_not_contains?: String | null
  slug_starts_with?: String | null
  slug_not_starts_with?: String | null
  slug_ends_with?: String | null
  slug_not_ends_with?: String | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  lastGameresultAt?: DateTime | null
  lastGameresultAt_not?: DateTime | null
  lastGameresultAt_in?: DateTime[] | DateTime | null
  lastGameresultAt_not_in?: DateTime[] | DateTime | null
  lastGameresultAt_lt?: DateTime | null
  lastGameresultAt_lte?: DateTime | null
  lastGameresultAt_gt?: DateTime | null
  lastGameresultAt_gte?: DateTime | null
  numResults?: Int | null
  numResults_not?: Int | null
  numResults_in?: Int[] | Int | null
  numResults_not_in?: Int[] | Int | null
  numResults_lt?: Int | null
  numResults_lte?: Int | null
  numResults_gt?: Int | null
  numResults_gte?: Int | null
  numPlayers?: Int | null
  numPlayers_not?: Int | null
  numPlayers_in?: Int[] | Int | null
  numPlayers_not_in?: Int[] | Int | null
  numPlayers_lt?: Int | null
  numPlayers_lte?: Int | null
  numPlayers_gt?: Int | null
  numPlayers_gte?: Int | null
  numGames?: Int | null
  numGames_not?: Int | null
  numGames_in?: Int[] | Int | null
  numGames_not_in?: Int[] | Int | null
  numGames_lt?: Int | null
  numGames_lte?: Int | null
  numGames_gt?: Int | null
  numGames_gte?: Int | null
  gameresults_every?: GameresultWhereInput | null
  gameresults_some?: GameresultWhereInput | null
  gameresults_none?: GameresultWhereInput | null
  players_every?: PlayerWhereInput | null
  players_some?: PlayerWhereInput | null
  players_none?: PlayerWhereInput | null
  members_every?: TournamentMemberWhereInput | null
  members_some?: TournamentMemberWhereInput | null
  members_none?: TournamentMemberWhereInput | null
  invitations_every?: InvitationWhereInput | null
  invitations_some?: InvitationWhereInput | null
  invitations_none?: InvitationWhereInput | null
}

export interface TournamentWhereUniqueInput {
  id?: ID_Input | null
  slug?: String | null
}

export interface UserCreateInput {
  id?: ID_Input | null
  auth0id: String
  username: String
  avatar: String
  tournaments?: TournamentMemberCreateManyWithoutUserInput | null
}

export interface UserCreateOneInput {
  create?: UserCreateInput | null
  connect?: UserWhereUniqueInput | null
}

export interface UserCreateOneWithoutTournamentsInput {
  create?: UserCreateWithoutTournamentsInput | null
  connect?: UserWhereUniqueInput | null
}

export interface UserCreateWithoutTournamentsInput {
  id?: ID_Input | null
  auth0id: String
  username: String
  avatar: String
}

export interface UserSubscriptionWhereInput {
  AND?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  OR?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  NOT?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: UserWhereInput | null
}

export interface UserUpdateDataInput {
  auth0id?: String | null
  username?: String | null
  avatar?: String | null
  tournaments?: TournamentMemberUpdateManyWithoutUserInput | null
}

export interface UserUpdateInput {
  auth0id?: String | null
  username?: String | null
  avatar?: String | null
  tournaments?: TournamentMemberUpdateManyWithoutUserInput | null
}

export interface UserUpdateManyMutationInput {
  auth0id?: String | null
  username?: String | null
  avatar?: String | null
}

export interface UserUpdateOneRequiredInput {
  create?: UserCreateInput | null
  connect?: UserWhereUniqueInput | null
  update?: UserUpdateDataInput | null
  upsert?: UserUpsertNestedInput | null
}

export interface UserUpdateOneRequiredWithoutTournamentsInput {
  create?: UserCreateWithoutTournamentsInput | null
  connect?: UserWhereUniqueInput | null
  update?: UserUpdateWithoutTournamentsDataInput | null
  upsert?: UserUpsertWithoutTournamentsInput | null
}

export interface UserUpdateWithoutTournamentsDataInput {
  auth0id?: String | null
  username?: String | null
  avatar?: String | null
}

export interface UserUpsertNestedInput {
  update: UserUpdateDataInput
  create: UserCreateInput
}

export interface UserUpsertWithoutTournamentsInput {
  update: UserUpdateWithoutTournamentsDataInput
  create: UserCreateWithoutTournamentsInput
}

export interface UserWhereInput {
  AND?: UserWhereInput[] | UserWhereInput | null
  OR?: UserWhereInput[] | UserWhereInput | null
  NOT?: UserWhereInput[] | UserWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  auth0id?: String | null
  auth0id_not?: String | null
  auth0id_in?: String[] | String | null
  auth0id_not_in?: String[] | String | null
  auth0id_lt?: String | null
  auth0id_lte?: String | null
  auth0id_gt?: String | null
  auth0id_gte?: String | null
  auth0id_contains?: String | null
  auth0id_not_contains?: String | null
  auth0id_starts_with?: String | null
  auth0id_not_starts_with?: String | null
  auth0id_ends_with?: String | null
  auth0id_not_ends_with?: String | null
  username?: String | null
  username_not?: String | null
  username_in?: String[] | String | null
  username_not_in?: String[] | String | null
  username_lt?: String | null
  username_lte?: String | null
  username_gt?: String | null
  username_gte?: String | null
  username_contains?: String | null
  username_not_contains?: String | null
  username_starts_with?: String | null
  username_not_starts_with?: String | null
  username_ends_with?: String | null
  username_not_ends_with?: String | null
  avatar?: String | null
  avatar_not?: String | null
  avatar_in?: String[] | String | null
  avatar_not_in?: String[] | String | null
  avatar_lt?: String | null
  avatar_lte?: String | null
  avatar_gt?: String | null
  avatar_gte?: String | null
  avatar_contains?: String | null
  avatar_not_contains?: String | null
  avatar_starts_with?: String | null
  avatar_not_starts_with?: String | null
  avatar_ends_with?: String | null
  avatar_not_ends_with?: String | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  tournaments_every?: TournamentMemberWhereInput | null
  tournaments_some?: TournamentMemberWhereInput | null
  tournaments_none?: TournamentMemberWhereInput | null
}

export interface UserWhereUniqueInput {
  id?: ID_Input | null
  auth0id?: String | null
}

/*
 * An object with an ID

 */
export interface Node {
  id: ID_Output
}

export interface AggregateBggInfo {
  count: Int
}

export interface AggregateFile {
  count: Int
}

export interface AggregateGameresult {
  count: Int
}

export interface AggregateInvitation {
  count: Int
}

export interface AggregatePlayer {
  count: Int
}

export interface AggregateScore {
  count: Int
}

export interface AggregateTournament {
  count: Int
}

export interface AggregateTournamentMember {
  count: Int
}

export interface AggregateUser {
  count: Int
}

export interface BatchPayload {
  count: Long
}

export interface BggInfo extends Node {
  bggid: Int
  createdAt: DateTime
  description: String
  gameresults?: Array<Gameresult> | null
  id: ID_Output
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  updatedAt: DateTime
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

/*
 * A connection to a list of items.

 */
export interface BggInfoConnection {
  pageInfo: PageInfo
  edges: Array<BggInfoEdge | null>
  aggregate: AggregateBggInfo
}

/*
 * An edge in a connection.

 */
export interface BggInfoEdge {
  node: BggInfo
  cursor: String
}

export interface BggInfoPreviousValues {
  bggid: Int
  createdAt: DateTime
  description: String
  id: ID_Output
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  updatedAt: DateTime
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

export interface BggInfoSubscriptionPayload {
  mutation: MutationType
  node?: BggInfo | null
  updatedFields?: Array<String> | null
  previousValues?: BggInfoPreviousValues | null
}

export interface File extends Node {
  contentType: String
  createdAt: DateTime
  id: ID_Output
  name: String
  secret: String
  size: Int
  updatedAt: DateTime
  url: String
}

/*
 * A connection to a list of items.

 */
export interface FileConnection {
  pageInfo: PageInfo
  edges: Array<FileEdge | null>
  aggregate: AggregateFile
}

/*
 * An edge in a connection.

 */
export interface FileEdge {
  node: File
  cursor: String
}

export interface FilePreviousValues {
  contentType: String
  createdAt: DateTime
  id: ID_Output
  name: String
  secret: String
  size: Int
  updatedAt: DateTime
  url: String
}

export interface FileSubscriptionPayload {
  mutation: MutationType
  node?: File | null
  updatedFields?: Array<String> | null
  previousValues?: FilePreviousValues | null
}

export interface Gameresult extends Node {
  bggInfo?: BggInfo | null
  createdAt: DateTime
  id: ID_Output
  numPlayers?: Int | null
  scores?: Array<Score> | null
  time: DateTime
  tournament: Tournament
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface GameresultConnection {
  pageInfo: PageInfo
  edges: Array<GameresultEdge | null>
  aggregate: AggregateGameresult
}

/*
 * An edge in a connection.

 */
export interface GameresultEdge {
  node: Gameresult
  cursor: String
}

export interface GameresultPreviousValues {
  createdAt: DateTime
  id: ID_Output
  numPlayers?: Int | null
  time: DateTime
  updatedAt: DateTime
}

export interface GameresultSubscriptionPayload {
  mutation: MutationType
  node?: Gameresult | null
  updatedFields?: Array<String> | null
  previousValues?: GameresultPreviousValues | null
}

export interface Invitation extends Node {
  id: ID_Output
  token: String
  state: InvitationState
  email: String
  inviter: User
  tournament: Tournament
  createdAt: DateTime
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface InvitationConnection {
  pageInfo: PageInfo
  edges: Array<InvitationEdge | null>
  aggregate: AggregateInvitation
}

/*
 * An edge in a connection.

 */
export interface InvitationEdge {
  node: Invitation
  cursor: String
}

export interface InvitationPreviousValues {
  id: ID_Output
  token: String
  state: InvitationState
  email: String
  createdAt: DateTime
  updatedAt: DateTime
}

export interface InvitationSubscriptionPayload {
  mutation: MutationType
  node?: Invitation | null
  updatedFields?: Array<String> | null
  previousValues?: InvitationPreviousValues | null
}

/*
 * Information about pagination in a connection.

 */
export interface PageInfo {
  hasNextPage: Boolean
  hasPreviousPage: Boolean
  startCursor?: String | null
  endCursor?: String | null
}

export interface Player extends Node {
  createdAt: DateTime
  id: ID_Output
  name: String
  scores?: Array<Score> | null
  tournament: Tournament
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface PlayerConnection {
  pageInfo: PageInfo
  edges: Array<PlayerEdge | null>
  aggregate: AggregatePlayer
}

/*
 * An edge in a connection.

 */
export interface PlayerEdge {
  node: Player
  cursor: String
}

export interface PlayerPreviousValues {
  createdAt: DateTime
  id: ID_Output
  name: String
  updatedAt: DateTime
}

export interface PlayerSubscriptionPayload {
  mutation: MutationType
  node?: Player | null
  updatedFields?: Array<String> | null
  previousValues?: PlayerPreviousValues | null
}

export interface Score extends Node {
  createdAt: DateTime
  gameresult?: Gameresult | null
  id: ID_Output
  player: Player
  score: Int
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface ScoreConnection {
  pageInfo: PageInfo
  edges: Array<ScoreEdge | null>
  aggregate: AggregateScore
}

/*
 * An edge in a connection.

 */
export interface ScoreEdge {
  node: Score
  cursor: String
}

export interface ScorePreviousValues {
  createdAt: DateTime
  id: ID_Output
  score: Int
  updatedAt: DateTime
}

export interface ScoreSubscriptionPayload {
  mutation: MutationType
  node?: Score | null
  updatedFields?: Array<String> | null
  previousValues?: ScorePreviousValues | null
}

export interface Tournament extends Node {
  id: ID_Output
  slug: String
  gameresults?: Array<Gameresult> | null
  name: String
  players?: Array<Player> | null
  members?: Array<TournamentMember> | null
  invitations?: Array<Invitation> | null
  createdAt: DateTime
  updatedAt: DateTime
  lastGameresultAt?: DateTime | null
  numResults: Int
  numPlayers: Int
  numGames: Int
}

/*
 * A connection to a list of items.

 */
export interface TournamentConnection {
  pageInfo: PageInfo
  edges: Array<TournamentEdge | null>
  aggregate: AggregateTournament
}

/*
 * An edge in a connection.

 */
export interface TournamentEdge {
  node: Tournament
  cursor: String
}

export interface TournamentMember extends Node {
  id: ID_Output
  tournament: Tournament
  user: User
  role: TournamentMemberRole
  createdAt: DateTime
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface TournamentMemberConnection {
  pageInfo: PageInfo
  edges: Array<TournamentMemberEdge | null>
  aggregate: AggregateTournamentMember
}

/*
 * An edge in a connection.

 */
export interface TournamentMemberEdge {
  node: TournamentMember
  cursor: String
}

export interface TournamentMemberPreviousValues {
  id: ID_Output
  role: TournamentMemberRole
  createdAt: DateTime
  updatedAt: DateTime
}

export interface TournamentMemberSubscriptionPayload {
  mutation: MutationType
  node?: TournamentMember | null
  updatedFields?: Array<String> | null
  previousValues?: TournamentMemberPreviousValues | null
}

export interface TournamentPreviousValues {
  id: ID_Output
  slug: String
  name: String
  createdAt: DateTime
  updatedAt: DateTime
  lastGameresultAt?: DateTime | null
  numResults: Int
  numPlayers: Int
  numGames: Int
}

export interface TournamentSubscriptionPayload {
  mutation: MutationType
  node?: Tournament | null
  updatedFields?: Array<String> | null
  previousValues?: TournamentPreviousValues | null
}

export interface User extends Node {
  id: ID_Output
  auth0id: String
  username: String
  avatar: String
  tournaments?: Array<TournamentMember> | null
  createdAt: DateTime
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface UserConnection {
  pageInfo: PageInfo
  edges: Array<UserEdge | null>
  aggregate: AggregateUser
}

/*
 * An edge in a connection.

 */
export interface UserEdge {
  node: User
  cursor: String
}

export interface UserPreviousValues {
  id: ID_Output
  auth0id: String
  username: String
  avatar: String
  createdAt: DateTime
  updatedAt: DateTime
}

export interface UserSubscriptionPayload {
  mutation: MutationType
  node?: User | null
  updatedFields?: Array<String> | null
  previousValues?: UserPreviousValues | null
}

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean

export type DateTime = Date | string

/*
The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point).
*/
export type Float = number

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number
export type ID_Output = string

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.
*/
export type Int = number

/*
The `Long` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
*/
export type Long = string

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string
