const jwt = require('jsonwebtoken')
const jwks = require('jwks-rsa')

const jwksClient = jwks({
  jwksUri: 'https://shpeely.auth0.com/.well-known/jwks.json'
})

export function getKey(header, callback) {
  jwksClient.getSigningKey(header.kid, (err, key) => {
    const signingKey = key.publicKey || key.rsaPublicKey
    callback(null, signingKey)
  })
}

export const validateAndParseIdToken = idToken =>
  new Promise((resolve, reject) => {
    const { header, payload } = jwt.decode(idToken, { complete: true })
    if (!header || !payload) reject(new Error('Invalid Token'))

    jwt.verify(idToken, getKey, { algorithms: ['RS256'] }, (err, decoded) => {
      if (err) reject(new Error(`jwt verify error: ${err.message}`))
      resolve(decoded)
    })
  })
