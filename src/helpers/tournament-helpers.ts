import {
  BggInfo,
  BggInfoConnection,
  Gameresult,
  GameresultConnection,
  PlayerConnection,
  Prisma
} from '../generated/prisma'
import * as logger from 'winston'

export async function updateTournamentMetaData(prisma: Prisma, slug: string) {
  try {
    const {
      data: {
        numPlayers: {
          aggregate: { count: playerCount }
        },
        numResults: {
          aggregate: { count: resultCount }
        },
        numGames: {
          aggregate: { count: gamesCount }
        },
        lastGameresults
      }
    } = await prisma.request<{
      data: {
        numPlayers: PlayerConnection
        numResults: GameresultConnection
        numGames: BggInfoConnection
        lastGameresults: Gameresult[]
      }
    }>(
      `
      query($slug: String!) {
      	lastGameresults: gameresults(where: {tournament: {slug: $slug}}, orderBy: time_DESC, first: 1) {
					time
				}
				
        numGames: bggInfoesConnection(where: {gameresults_some: {tournament: {slug: $slug}}}) {
           aggregate {
            count
          }
        }
				
        numResults: gameresultsConnection(where: {
          tournament:{
            slug: $slug
          }
        }) {
           aggregate {
            count
          }
        }
        
        numPlayers: playersConnection(where: {
          tournament:{
            slug: $slug
          }
        }) {
           aggregate {
            count
          }
        }
      }
    `,
      { slug }
    )

    await prisma.mutation.updateTournament({
      where: {
        slug
      },
      data: {
        numPlayers: playerCount,
        numResults: resultCount,
        numGames: gamesCount,
        lastGameresultAt: lastGameresults.length
          ? lastGameresults[0].time
          : null
      }
    })

    logger.debug('Successfully updated tournament meta data')
  } catch (err) {
    logger.error(`Failed to update tournament meta data: ${err.message}`)
  }
}
