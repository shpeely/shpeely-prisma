import * as logger from 'winston'
import { Auth0, UserProfile } from '../services/auth0'
import { Prisma, User } from '../generated/prisma'

async function loadProfileDetails(
  idToken: string
): Promise<UserProfile | null> {
  // add more profile data if the token allows it
  try {
    return await Auth0.loadUserProfile(idToken)
  } catch (err) {
    logger.debug('Could not load profile information; ', err.message)

    return Promise.resolve(null)
  }
}

async function createPrismaUser(db: Prisma, userToken, profile: UserProfile) {
  return await db.mutation.createUser({
    data: {
      username:
        profile.name ||
        profile.nickname ||
        `User ${Math.round(Math.random() * 10000000)}`,
      avatar: profile.picture,
      auth0id: userToken.sub
    }
  })
}

async function createNewUser(
  db: Prisma,
  auth0id: string,
  profile: UserProfile
): Promise<User> {
  logger.debug(`No user found yet for auth0id ${auth0id}. Creating new user...`)

  logger.debug(`Loading user profile for user ${auth0id}`)

  const detailedProfile = await loadProfileDetails(auth0id)

  const effectiveProfile = {
    ...profile,
    ...(detailedProfile || {})
  }

  logger.debug(`Loaded profile for auth0id ${auth0id}`)

  let user: User

  try {
    logger.debug(`Creating user ${auth0id} in prisma`)
    user = await db.mutation.createUser({
      data: {
        username:
          effectiveProfile.name ||
          effectiveProfile.nickname ||
          `User ${Math.round(Math.random() * 10000000)}`,
        avatar: effectiveProfile.picture,
        auth0id
      }
    })
  } catch (err) {
    /*
    If two requests are made in a short time it's possible that it tries to create the same
    user twice. The second request will because of a unique constraint violation and enter
    this branch. In that case we simply try to load the user again...
    Specifically checking for the unique constraint would be better but hey let's live a little.
     */
    logger.warn('Failed to create user. Perhaps it was already created?')

    user = await db.query.user({ where: { auth0id } })
  }

  return user
}

export const getUser = async (req, res, next, db) => {
  if (!req.user) {
    return next()
  }

  const auth0id = req.user.sub

  let user = await db.query.user({ where: { auth0id } })

  if (!user) {
    user = await createNewUser(db, auth0id, req.user)
  }

  req.user = user
  next()
}
