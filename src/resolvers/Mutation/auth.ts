import { validateAndParseIdToken } from '../../helpers/authHelpers'
import { Auth0, UserProfile } from '../../services/auth0'
import { Prisma } from '../../generated/prisma'
import * as logger from 'winston'

async function createPrismaUser(
  ctx: { db: Prisma },
  userToken,
  profile: UserProfile
) {
  const user = await ctx.db.mutation.createUser({
    data: {
      username:
        profile.name ||
        profile.nickname ||
        `User ${Math.round(Math.random() * 10000000)}`,
      avatar: profile.picture,
      auth0id: userToken.sub
    }
  })
  return user
}

async function loadProfileDetails(
  idToken: string
): Promise<UserProfile | null> {
  // add more profile data if the token allows it
  try {
    return await Auth0.loadUserProfile(idToken)
  } catch (err) {
    logger.debug('Could not load profile information; ', err.message)

    return Promise.resolve(null)
  }
}

export const auth = {
  /**
   * This resolver is no longer needed as users are automatically created without having to call this resolver.
   * The resolver is kept here only for downwards compatibility.
   *
   * @param parent
   * @param idToken
   * @param ctx
   * @param info
   *
   * @deprecated
   */
  async authenticate(parent, { idToken }, ctx, info) {
    logger.debug('Authenticating user')
    let userToken = null
    try {
      userToken = await validateAndParseIdToken(idToken)
    } catch (err) {
      throw new Error(err.message)
    }

    logger.debug('Parsed user token', userToken)

    const profile: UserProfile = {
      given_name: userToken.given_name,
      family_name: userToken.family_name,
      name: userToken.name,
      nickname: userToken.nickname,
      picture: userToken.picture,
      gender: userToken.gender,
      updated_at: userToken.updated_at, // ISO-8601
      email: userToken.email,
      email_verified: userToken.email_verified
    }

    const auth0id = userToken.sub

    logger.debug(`Read auth0id from the user token: ${auth0id}`)

    let user = await ctx.db.query.user({ where: { auth0id } }, info)

    if (!user) {
      logger.info(
        `No user found yet for auth0id ${auth0id}. Creating new user...`
      )

      const profileDetails = await loadProfileDetails(auth0id)

      logger.info(`Loaded profile for auth0id ${auth0id}`)

      try {
        user = await createPrismaUser(ctx, userToken, {
          ...profile,
          ...(profileDetails || {})
        })
      } catch (err) {
        /*
    		If two requests are made in a short time it's possible that it tries to create the same
    		user twice. The second request will because of a unique constraint violation and enter
    		this branch. In that case we simply try to load the user again...
    		Specifically checking for the unique constraint would be better but hey let's live a little.
    		 */
        logger.warn('Failed to create user. Perhaps it was already created?')

        return ctx.db.query.user({ where: { auth0id } }, info)
      }
    }

    return user
  }
}
