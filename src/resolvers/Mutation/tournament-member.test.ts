import { TournamentMember } from '../../generated/prisma'

jest.mock('../../services/mailer')

import server from '../../server'
import { GraphQlTestClient } from '../../test/graphQlTestClient'
import { GraphQlError } from '../../errors'
import {
  acceptInvitation,
  createInvitation,
  createRandomTournament,
  createTournament,
  expectGraphQlError,
  loadTournamentMembers,
  setMemberRole
} from '../../test/testUtils'

describe('Tournament Member Mutations', () => {
  const testuser1 = {
    username: process.env.TEST_USER,
    password: process.env.TEST_PASSWORD
  }

  const testuser2 = {
    username: process.env.TEST_USER2,
    password: process.env.TEST_PASSWORD2
  }

  const port = 4002

  const sendMailMock = jest.fn()

  beforeEach(() => {
    const { Mailer } = require('../../services/mailer')
    Mailer.__setSendMailFn(sendMailMock)
  })

  afterEach(() => {
    sendMailMock.mockReset()
  })

  describe('Anonymous', () => {
    let client
    let tournament

    beforeAll(async () => {
      const app = await server.start({ port })

      client = new GraphQlTestClient(app)
      await client.authenticate(testuser1.username, testuser1.password)
      tournament = await createTournament(client, 'Some Tournament')
      client.unauthenticate()
    })

    afterAll(() => client.close())

    test('Should throw a login required error', async () => {
      const { slug } = tournament

      await expectGraphQlError(
        async () => await setMemberRole(client, 'xxx', 'ADMIN'),
        GraphQlError.LOGIN_REQUIRED
      )
    })
  })

  describe('Authenticated', () => {
    let client
    let tournament
    let initialMembers: TournamentMember[]

    beforeAll(async () => {
      const app = await server.start({ port })
      client = new GraphQlTestClient(app)
    })

    beforeEach(async () => {
      await client.authenticate(testuser1.username, testuser1.password)
      tournament = await createRandomTournament(client)
      initialMembers = await loadTournamentMembers(client, tournament.slug)
    })

    afterAll(() => client.close())

    test('Should throw an error if a non-admin tries to change a role', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'info@shpeely.com'
      )

      await client.authenticate(testuser2.username, testuser2.password)
      await acceptInvitation(client, invitation.token)

      await expectGraphQlError(
        async () => await setMemberRole(client, initialMembers[0].id, 'MEMBER'),
        GraphQlError.NOT_ALLOWED_BASED_ON_ROLE
      )
    })

    test('Should be possible for an admin to change a tournament member role', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'info@shpeely.com'
      )

      await client.authenticate(testuser2.username, testuser2.password)
      await acceptInvitation(client, invitation.token)
      let members = await loadTournamentMembers(client, tournament.slug)

      // find the non-admin and make him admin
      const member = members.find(x => x.role !== 'ADMIN')

      await client.authenticate(testuser1.username, testuser1.password)
      await setMemberRole(client, member.id, 'ADMIN')

      // now all the members should be admin
      members = await loadTournamentMembers(client, tournament.slug)

      expect(members.map(x => x.role)).toEqual(['ADMIN', 'ADMIN'])
    })
  })
})
