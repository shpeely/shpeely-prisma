import { GraphQLResolveInfo } from 'graphql'
import { Context } from '../../utils'
import {
  TournamentMember,
  TournamentMemberWhereUniqueInput,
  TournamentMemberUpdateInput
} from '../../generated/prisma'

export const tournamentMember = {
  updateTournamentMember: async (
    parent: any,
    args: {
      where: TournamentMemberWhereUniqueInput
      data: TournamentMemberUpdateInput
    },
    ctx: Context,
    info: GraphQLResolveInfo
  ): Promise<TournamentMember | null> => {
    return ctx.db.mutation.updateTournamentMember(args, info)
  }
}
