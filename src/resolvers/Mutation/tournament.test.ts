jest.mock('../../services/mailer')

import server from '../../server'
import { GraphQlTestClient } from '../../test/graphQlTestClient'
import { GraphQlError } from '../../errors'
import { createTournament, expectGraphQlError } from '../../test/testUtils'

describe('Tournament Mutations', () => {
  const port = 4001

  const testuser1 = {
    username: process.env.TEST_USER,
    password: process.env.TEST_PASSWORD
  }

  describe('Anonymous', () => {
    let client

    beforeAll(async () => {
      const app = await server.start({ port })
      client = new GraphQlTestClient(app)
    })

    afterAll(() => client.close())

    test('Should throw a login required error', async () => {
      const query = `
				mutation ($name: String!) {
					createTournament(data: {name: $name}) {
						slug
					}
				}
			`
      const variables = {
        name: 'Test Tournament'
      }

      expectGraphQlError(
        async () => await client.request(query, variables),
        GraphQlError.LOGIN_REQUIRED
      )
    })
  })

  describe('Authenticated', () => {
    let client

    beforeAll(async () => {
      const app = await server.start({ port })
      client = new GraphQlTestClient(app)
      await client.authenticate(testuser1.username, testuser1.password)
    })

    afterAll(() => client.close())

    test('Should create a tournament', async () => {
      const name = 'Test Tournament'

      const tournament = await createTournament(client, name)

      expect(tournament).not.toBeNull()
      expect(tournament.members).toHaveLength(1)
      expect(tournament.members[0].role).toBe('ADMIN')
      expect(tournament.name).toBe(name)
    })

    test('Should create unique slugs fot tournaments with the same name', async () => {
      const tournamentName = 'Test Tournament'

      const { name, slug } = await createTournament(client, tournamentName)
      const { name: name2, slug: slug2 } = await await createTournament(
        client,
        name
      )

      expect(name).toEqual(name2)
      expect(slug).not.toEqual(slug2)
    })
  })
})
