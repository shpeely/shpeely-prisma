export class MockMailerApi {
  sendMail: Function = (...args) => {
    console.log('Sent mail', args)
  }

  __setSendMailFn(fn: Function) {
    this.sendMail = fn
  }
}

export const Mailer = new MockMailerApi()

export default Mailer
