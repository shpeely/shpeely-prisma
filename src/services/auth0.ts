import axios from 'axios'
import * as logger from 'winston'

export enum Auth0Provider {
  FACEBOOK = 'facebook',
  GOOGLE = 'google'
}

export interface Identity {
  access_token: string
  provider: Auth0Provider
  isSocial: boolean
  user_id: string
}

export interface UserProfile {
  given_name?: string
  family_name?: string
  name?: string
  nickname: string
  picture: string
  gender: string
  updated_at: string // ISO-8601
  email: string
  email_verified: boolean
  identities?: Identity[]
}

interface TokenPayload {
  access_token: string
  expires_in
  number
}

export class Auth0Api {
  private token?: string
  private tokenValidUntil?: Date

  constructor(
    private readonly auth0Domain = process.env.AUTH0_DOMAIN,
    private readonly clientSecret = process.env.AUTH0_MANAGEMENT_SECRET,
    private readonly clientId = process.env.AUTH0_MANAGEMENT_ID
  ) {}

  async loadUserProfile(auth0id: string): Promise<UserProfile> {
    if (!this.token || this.tokenValidUntil < new Date()) {
      logger.info('Auth0 management token is expired or does not exist')

      try {
        const { accessToken, validUntil } = await this.getToken()
        this.token = accessToken
        this.tokenValidUntil = validUntil
      } catch (err) {
        logger.error(`Failed to obtain management token: ${err.message}`)
      }
    }

    const { data } = await axios.get<UserProfile>(
      `https://${this.auth0Domain}/api/v2/users/${auth0id}`,
      {
        data: {
          include_fields: true
        },
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      }
    )

    // special case for facebook: Manually determine picture URL (the profile picture stored in
    // auth0 can expire)
    if (data.identities) {
      const facebookIdentity = data.identities.find(
        i => i.provider === Auth0Provider.FACEBOOK
      )
      if (facebookIdentity) {
        data.picture = `https://graph.facebook.com/v2.9/${
          facebookIdentity.user_id
        }/picture?type=large`
      }
    }

    return data
  }

  private async getToken(): Promise<{ accessToken: string; validUntil: Date }> {
    logger.info('Creating token for auth0 management API')

    const {
      data: { access_token: accessToken, expires_in: expiresIn }
    } = await axios.post<TokenPayload>(
      `https://${this.auth0Domain}/oauth/token`,
      {
        grant_type: 'client_credentials',
        client_id: this.clientId,
        client_secret: this.clientSecret,
        audience: `https://${this.auth0Domain}/api/v2/`
      },
      {
        headers: {
          'content-type': 'application/json'
        }
      }
    )

    const validUntil = new Date(
      new Date().getTime() + (expiresIn - 3600) * 1000
    )

    logger.debug(
      `Auth0 management token is valid until ${validUntil.toISOString()}`
    )

    return {
      accessToken,
      validUntil
    }
  }
}

export const Auth0 = new Auth0Api()
