import * as _ from 'lodash'
import * as querystring from 'querystring'
import * as striptags from 'striptags'
import * as he from 'he'
import * as cheerio from 'cheerio'
import * as vm from 'vm'
import * as logger from 'winston'
import axios from 'axios'

// thumbnails
const THUMBNAIL_BASE_URL = 'https://cf.geekdo-images.com/images/pic'
const THUMBNAIL_EXT = 'jpg'
const THUMBNAIL_SIZE = {
  small: 't', // 150x150
  large: 'md' // 500x500
}

interface GameInfo {
  bggid: number
  description: string
  maxPlayers: number
  name: string
  ownedBy: number
  playTime: number
  rank: number
  rating: number
  thumbnail: string
  weight: number
  yearPublished: number
  lastReload: Date
}

interface GameSearchResult {
  bggid: number
  name: string
  year: number
}

// subset of data returned by BGG search
interface BggGameSearchResult {
  items: {
    objectid: string
    name: string
    yearpublished: number
  }[]
}

function createModel(bggInfo: any, bggStats: any): GameInfo {
  const bggid = bggInfo.item.objectid

  /**
   * special case for weight. Weight is a required field but not all games have a weight defined. For lack of a better
   * soltion, I'm setting the weight to 1 for now. In the future maybe it makes sense to filter such games out from
   * the beginning, or letting the user define a weight, or finding the weight of the base game if the game is an
   * expansion... Or some other solution...
   */
  let weight = _.get(bggStats, 'item.polls.boardgameweight.averageweight')

  if (!weight) {
    console.debug('Game with id %s has no weight. Setting weight of 1', bggid)
    weight = 1
  }

  try {
    const overallRankInfo = bggStats.item.rankinfo.find(
      x => x.subdomain === null
    )

    return {
      bggid,
      weight,
      name: bggInfo.item.name,
      lastReload: new Date(),
      maxPlayers: Number(bggInfo.item.maxplayers),
      thumbnail: bggInfo.item.images.thumb,
      description: he.decode(striptags(bggInfo.item.description)),
      rating: Number(overallRankInfo.baverage),
      rank: Number(overallRankInfo.rank),
      ownedBy: Number(bggStats.item.stats.numowned),
      playTime: Number(bggInfo.item.maxplaytime),
      yearPublished: Number(bggInfo.item.yearpublished)
    }
  } catch (err) {
    console.error('Failed to create model from BGG data.', err)
    console.error(
      'Incopatible data received from BGG:\n %s',
      'bggInfo object: ',
      JSON.stringify(bggInfo, null, 2),
      'bggStats object: ',
      JSON.stringify(bggStats, null, 2)
    )
    throw new Error('Failed to parse data from BGG API')
  }
}

function fetchStats(bggUrl, id) {
  // Unfortunately the 'dynamicinfo' endpoint does not seem to be accessible anymore from the outside.
  // The data is already loaded with the HTML and assigned to a global GEEK variable. So in order to get to the data,
  // I need to load the HTML page, evaluate the scripts in a sandboxed vm and can then access the global GEEK variable.

  const globals = {
    GEEK: null
  }
  const context = vm.createContext(globals)

  return fetch(`${bggUrl}/boardgame/${id}`)
    .then(res => res.text())
    .then(cheerio.load)
    .then($ => {
      // iterate over all scripts and evaluate them in a sandbox
      $('script').each((i, script) => {
        try {
          const scriptContent = script.children[0].data
          const sandbox = new vm.Script(scriptContent)
          sandbox.runInContext(context)
        } catch (err) {
          // ignore
        }
      })

      if (!globals.GEEK || !globals.GEEK.geekitemPreload) {
        console.error(`Failed to load stats for game ${id} from the BGG site.`)
        return null
      }

      return globals.GEEK.geekitemPreload
    })
}

function fetchInfo(bggUrl, id) {
  return fetch(`${bggUrl}/api/geekitems?objecttype=thing&objectid=${id}`).then(
    res => res.json()
  )
}

export class BoardGameGeekApi {
  private bggUrl: string

  constructor() {
    this.bggUrl = process.env.BGG_URL || 'https://boardgamegeek.com'
  }

  getGameInfo(bggid: number): Promise<GameInfo> {
    return Promise.all([
      fetchInfo(this.bggUrl, bggid),
      fetchStats(this.bggUrl, bggid)
    ]).then(([info, stats]) => createModel(info, stats))
  }

  searchGame(search: string, maxResults: number = 20) {
    const query = querystring.stringify({
      q: search,
      showcount: maxResults
    })

    return axios
      .get<BggGameSearchResult>(`${this.bggUrl}/search/boardgame?${query}`)
      .then(({ data }) =>
        data.items.map(item => ({
          bggid: Number(item.objectid),
          year: item.yearpublished,
          name: item.name
        }))
      )
  }
}

export const BoardGameGeek = new BoardGameGeekApi()
