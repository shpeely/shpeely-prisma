import * as Mailgun from 'mailgun-js'
import * as logger from 'winston'

export class MailClient {
  private mailer: Mailgun
  private sender: string = 'Shpeely <no-reply@shpeely.com>'

  constructor() {
    const apiKey = process.env.MAILGUN_API_KEY
    const domain = process.env.MAILGUN_DOMAIN

    this.mailer = Mailgun({
      apiKey,
      domain
    })
  }

  sendMail(to: string, subject: string, html: string): Promise<any> {
    logger.info(`Sending email to ${to}.`)

    return this.mailer.messages().send({
      from: this.sender,
      to,
      subject,
      html
    })
  }
}

export const Mailer = new MailClient()
