import { updateProfilePictures } from './update-profile-pictures'
import { Prisma } from '../generated/prisma'
import * as logger from 'winston'
import { updateTournamentMetaDataStartupTask } from './update-tournament-meta-data'

const DAY_MS = 1000 * 60 * 60 * 24

export async function start(db: Prisma) {
  // startup tasks...
  await updateTournamentMetaDataStartupTask(db)

  // repeated tasks...
  async function repeatedTasks() {
    try {
      await updateProfilePictures(db)
    } catch (e) {
      logger.error(`task failed: ${e.message}`, e.message)
    } finally {
      setTimeout(repeatedTasks, DAY_MS * 3)
    }
  }

  await repeatedTasks()
}
