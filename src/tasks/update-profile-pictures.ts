import { Auth0 } from '../services/auth0'
import { Prisma } from '../generated/prisma'
import * as logger from 'winston'

export async function updateProfilePictures(db: Prisma) {
  logger.info('Updating profile picures...')

  const users = await db.query.users({}, `{ auth0id }`)

  for (let user of users) {
    const { picture } = await Auth0.loadUserProfile(user.auth0id)

    await db.mutation.updateUser(
      {
        where: { auth0id: user.auth0id },
        data: { avatar: picture }
      },
      '{ avatar }'
    )
  }
}
