import { Prisma } from '../generated/prisma'
import * as logger from 'winston'
import { updateTournamentMetaData } from '../helpers/tournament-helpers'

export async function updateTournamentMetaDataStartupTask(db: Prisma) {
  logger.info('Updating tournament meta data...')

  const tournaments = await db.query.tournaments({}, '{ slug }')

  for (let tournament of tournaments) {
    try {
      await updateTournamentMetaData(db, tournament.slug)
    } catch (err) {
      logger.error(
        `Failed to update meta data of tournament ${tournament.slug}: ${
          err.message
        }`
      )
    }
  }

  logger.info('Finished updating tournament meta data')
}
